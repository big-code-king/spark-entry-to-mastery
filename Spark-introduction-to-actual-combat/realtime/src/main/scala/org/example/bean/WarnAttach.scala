package org.example.bean

/**
 * 警告附加
 * @param appId
 * @param businessTime
 * @param deviceId
 * @param fileCount
 * @param fileIndex
 * @param filePath
 * @param vehicleColor
 * @param vehicleNo
 * @param vehicleType
 * @param warnSeq
 * @param warnTime
 * @param INFO_ID
 * @param DATA_TYPE
 */
case class WarnAttach(
                   var appId:String,
                   var businessTime:String,
                   var deviceId:String,
                   var fileCount: String,
                   var fileIndex:String,
                   var filePath:String,
                   var vehicleColor:String,
                   var vehicleNo:String,
                   var vehicleType:String,
                   var warnSeq:String,
                   var warnTime:String,
                   var INFO_ID:String,
                   var DATA_TYPE:String
                 )
