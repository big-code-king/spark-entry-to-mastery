package org.example.bean

/**
 * 车GPS类
 * @param alarm
 * @param altitude
 * @param appId
 * @param businessTime
 * @param dataType
 * @param dateTime
 * @param direction
 * @param encrypt
 * @param lat
 * @param lon
 * @param msgId
 * @param state
 * @param vec1
 * @param vec2
 * @param vec3
 * @param vehicleColor
 * @param vehicleNo
 * @param dealDeptCode
 * @param enterpriseName
 * @param socialCreditCode
 */
case class CarGPS(
                   var alarm:String,  //报警状态
                   var altitude:String,  //海拔高度，单位为米(m)
                   var appId:String,  //智能视频企业监控平台的唯一标识号	10000
                   var businessTime: String,  //业务发生时间,格式yyyy-MM-dd HH:mm:ss
                   var dataType:String,  //子业务类型标识	0x1202
                   var dateTime:String,  //定位时间,格式yyyy-MM-dd HH:mm:ss
                   var direction:String,  //方向，0 ～ 359，单位为度(°)，正北为 0，顺时针
                   var encrypt:String,  //加密标识：1-已加密，0-未加密
                   var lat:String,  //纬度，单位为 1*10 - 6度
                   var lon:String,  //经度，单位为 1*10 - 6度
                   var msgId:String,  //业务类型标识	0x1202
                   var state:String,  //车辆状态
                   var vec1:String,  //速度，指卫星定位车载终端设备上传的行车速度信息，为必填项， 单位为千米每小时(km/ h)
                   var vec2:String,  //行驶记录速度，指车辆行驶记录设备上传的行车速度信息，单位 为千米每小时(km/ h)
                   var vec3:String,  //车辆当前总里程数，指车辆上传的行车里程数，单位为千米(km)
                   var vehicleColor:String,  //车牌颜色
                   var vehicleNo:String , //车牌号
                   var dealDeptCode: String, //处理部门编码（企业编码）
                   var enterpriseName:String, //企业名称
                   var socialCreditCode :String //社会信用代码
                 )
