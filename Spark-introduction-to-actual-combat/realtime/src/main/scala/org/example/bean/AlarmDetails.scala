package org.example.bean

/**
 * 报警详情类
 * @param alarmId
 * @param alarmday
 * @param appid
 * @param businesstime
 * @param datatype
 * @param districtcode
 * @param driverid
 * @param drivername
 * @param enterprisecode
 * @param enterprisename
 * @param eventtype
 * @param id
 * @param infoid
 * @param latitude
 * @param longitude
 * @param socialcreditcode
 * @param usenature
 * @param vehiclecolor
 * @param vehicleno
 * @param warningtypecode
 * @param warningtypename
 * @param warnsrc
 * @param warntime
 * @param warntype
 */
case class AlarmDetails(
                   var alarmId:String,
                   var alarmday:String,
                   var appid:String,
                   var businesstime: String,
                   var datatype:String,
                   var districtcode:String,
                   var driverid:String,
                   var drivername:String,
                   var enterprisecode:String,
                   var enterprisename:String,
                   var eventtype:String,
                   var id:String,
                   var infoid:String,
                   var latitude:String,
                   var longitude:String,
                   var socialcreditcode:String,
                   var usenature:String,
                   var vehiclecolor:String,
                   var vehicleno:String,
                   var warningtypecode:String,
                   var warningtypename:String,
                   var warnsrc:String,
                   var warntime:String,
                   var warntype:String
                       )
