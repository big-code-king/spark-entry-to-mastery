package org.example.bean

/**
 * 报警详情完整性
 * @param alarmId
 * @param alarmTime
 * @param enterpriseCode
 * @param id
 * @param imgCount
 * @param socialCreditCode
 * @param videoCount
 */
case class AlarmDetailsIntegrity(
                   var alarmId:String,
                   var alarmTime:String,
                   var enterpriseCode:String,
                   var id: String,
                   var imgCount:Int,
                   var socialCreditCode:String,
                   var videoCount:Int
                       )
