package org.example.service

import com.alibaba.fastjson.JSONObject
import org.example.bean.CarGPS
import org.example.common.Logging
import org.example.utils.{GPSUtil, IdWorker}
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import redis.clients.jedis.Jedis

/**
 * 处理隐患
 */
object ProduceHidden extends Logging{

  /**
   * 处理隐患
   * @param carGPS
   * @param jedis
   * @param industry
   * @param kafkaProducer
   * @param typeCode
   * @param content
   */
  def processHidden(hiddenName:String,carGPS: CarGPS, jedis: Jedis, industry: String,
                    kafkaProducer: KafkaProducer[String, String], typeCode: String,
                    content: String,keyOfRedis:String,yesTodayKeyOfRedis:String,
                    superviseConfigId: String,superviseRuleConfigId: String,generateObject:String,
                    generateObjectCode: String,transport_supervise_topic:String,tripStartTime:String,
                    generate_detail:String,hiddenRuleMap: Map[String, Boolean]): Unit = {
    info("rule:"+hiddenRuleMap.getOrElse(superviseConfigId,false))
    if (hiddenRuleMap.getOrElse(superviseConfigId,false)) {
      jedis.select(9)

      val todayStr: String = new DateTime().toString("yyyy-MM-dd")
      try {
        if (jedis.hget(keyOfRedis, carGPS.vehicleNo + "_" + carGPS.vehicleColor) == "1") {
          //break()
        } else { //redis没有存该车的过期记录
          val addr: (String, String, String, String) = GPSUtil.getAddressAndRoad(carGPS.lon.toDouble, carGPS.lat.toDouble)

          val inner_area = if (addr._1.contains("重庆市")) 1 else 0
          val jSONObject = new JSONObject()
          jSONObject.put("orderNumber", new IdWorker().nextId().toString)
          jSONObject.put("dealStatus", "1")
          jSONObject.put("generatedTime", carGPS.dateTime)
          jSONObject.put("content", content)
          jSONObject.put("superviseRuleConfigId", superviseRuleConfigId)
          jSONObject.put("industry", industry) //所属行业
          jSONObject.put("superviseConfigId", superviseConfigId)
          jSONObject.put("businessId", "1")
          jSONObject.put("generateObject", generateObject)
          jSONObject.put("generate_object_code", generateObjectCode)
          jSONObject.put("longitude", carGPS.lon)
          jSONObject.put("latitude", carGPS.lat)
          jSONObject.put("addressName", addr._1 + "_" + addr._4)
          jSONObject.put("inner_area", inner_area)
          jSONObject.put("dealDept", carGPS.enterpriseName)
          jSONObject.put("dealDeptCode", carGPS.dealDeptCode)
          jSONObject.put("deptCode", carGPS.dealDeptCode)
          jSONObject.put("typeCode", typeCode)
          jSONObject.put("vehicleNo", carGPS.vehicleNo)
          jSONObject.put("vehicleColor", if (carGPS.vehicleColor == "蓝色") "1"
          else if (carGPS.vehicleColor == "黄色") "2"
          else if (carGPS.vehicleColor == "黑色") "3"
          else if (carGPS.vehicleColor == "白色") "4"
          else if (carGPS.vehicleColor == "绿色") "5"

          else "9")
          jSONObject.put("driverStartTime", tripStartTime)
          jSONObject.put("generate_detail", generate_detail)

          //发送kafka
          kafkaProducer.send(new ProducerRecord[String, String](transport_supervise_topic, jSONObject.toJSONString))
          //将过期记录保存到redis
          jedis.hset(keyOfRedis, carGPS.vehicleNo + "_" + carGPS.vehicleColor, "1")
        }
        val yesterdayStr: String = DateTime.parse(todayStr, DateTimeFormat.forPattern("yyyy-MM-dd")).plusDays(-1).toString("yyyy-MM-dd")
        //删除昨天的redis数据
        if (jedis.exists(yesTodayKeyOfRedis)) {
          jedis.del(yesTodayKeyOfRedis)
        }

      } catch {
        case e: Exception =>
        //println("处理" + hiddenName + "隐患失败！")
      }
    }
  }

  /**
   * 处理路线隐患
   *
   * @param carGPS
   * @param industry
   * @param kafkaProducer
   * @param typeCode
   * @param content
   */
  def processRouteHidden(hiddenName:String,carGPS: CarGPS,  industry: String,
                         kafkaProducer: KafkaProducer[String, String], typeCode: String,
                         content: String,
                         superviseConfigId: String,superviseRuleConfigId: String,generateObject:String,
                         generateObjectCode: String,transport_supervise_topic:String,generate_detail:String,
                         startTime:String,endTime:String,
                         hiddenRuleMap: Map[String, Boolean]): Unit = {
    info("rule:"+hiddenRuleMap.getOrElse(superviseConfigId,false))
    if (hiddenRuleMap.getOrElse(superviseConfigId,false)) {
      info("生成隐患"+hiddenName)
      val todayStr: String = new DateTime().toString("yyyy-MM-dd")
      try {

        val addr: (String, String, String, String) = GPSUtil.getAddressAndRoad(carGPS.lon.toDouble, carGPS.lat.toDouble)
        val inner_area = if (addr._1.contains("重庆市")) 1 else 0

        val jSONObject = new JSONObject()
        jSONObject.put("orderNumber", new IdWorker().nextId().toString)
        jSONObject.put("dealStatus", "1")
        jSONObject.put("generatedTime", carGPS.dateTime)
        jSONObject.put("content", content)
        jSONObject.put("superviseRuleConfigId", superviseRuleConfigId)
        jSONObject.put("industry", industry) //所属行业
        jSONObject.put("superviseConfigId", superviseConfigId)
        jSONObject.put("businessId", "1")
        jSONObject.put("generateObject", generateObject)
        jSONObject.put("generate_object_code", generateObjectCode)
        jSONObject.put("longitude", carGPS.lon)
        jSONObject.put("latitude", carGPS.lat)
        jSONObject.put("addressName", addr._1 + "_" + addr._4)
        jSONObject.put("inner_area", inner_area)
        jSONObject.put("dealDept", carGPS.enterpriseName)
        jSONObject.put("dealDeptCode", carGPS.dealDeptCode)
        jSONObject.put("deptCode", carGPS.dealDeptCode)
        jSONObject.put("typeCode", typeCode)
        jSONObject.put("vehicleNo", carGPS.vehicleNo)
        jSONObject.put("vehicleColor", if (carGPS.vehicleColor == "蓝色") "1"
        else if (carGPS.vehicleColor == "黄色") "2"
        else if (carGPS.vehicleColor == "黑色") "3"
        else if (carGPS.vehicleColor == "白色") "4"
        else if (carGPS.vehicleColor == "绿色") "5"
        else "9")
        jSONObject.put("driverStartTime", startTime)
        jSONObject.put("driverEndTime", endTime)


        jSONObject.put("generate_detail", generate_detail)
        //发送kafka
        kafkaProducer.send(new ProducerRecord[String, String](transport_supervise_topic, jSONObject.toJSONString))
      } catch {
        case e: Exception =>
          info("处理" + hiddenName + "隐患失败！")
      }
    }
  }

}
