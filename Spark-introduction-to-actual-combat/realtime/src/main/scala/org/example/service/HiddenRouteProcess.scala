package org.example.service

import org.example.bean.CarGPS
import org.example.dao.{HiddenEvidence, HiddenTrajectory}
import org.example.service.ProduceHidden.processRouteHidden
import org.example.utils.RouteUtil.{getAddrNameType, getCharterLocationType, getLocationType}
import org.example.utils.{EsUtils, GPSUtil, Tools}
import io.searchbox.client.JestClient
import org.apache.kafka.clients.producer.KafkaProducer
import redis.clients.jedis.Jedis

import scala.collection.mutable.ArrayBuffer

object HiddenRouteProcess {
  /**
   * 准运证未按规定路线
   *
   * @param carGPS
   * @param jedis
   * @param typeCode
   * @return
   */
  def processPermitRoute(kafkaProducer: KafkaProducer[String, String],carGPS: CarGPS, jedis: Jedis,industry: String, typeCode: String,pass_time_interval: String,pass_route_location: String,permit_license_no:String,hiddenRuleMap: Map[String, Boolean]) = {
    //println("进入准运证路线周期判断")
    val passTimeArr = pass_time_interval.split(",")
    var routeWay = jedis.hget("muck:" + carGPS.vehicleNo + "_" + carGPS.vehicleColor + "_" + permit_license_no, "routeWay")
    var routetime = jedis.hget("muck:" + carGPS.vehicleNo + "_" + carGPS.vehicleColor + "_" + permit_license_no, "routeTime")
    var routeGPS = jedis.hget("muck:" + carGPS.vehicleNo + "_" + carGPS.vehicleColor + "_" + permit_license_no, "routeGPS")

    val routePointArr = pass_route_location.split("\\|")
    if (routeWay==null){
      //println("车辆第一次准运证路线记录"+pass_route_location)
      val location=getLocationType(routePointArr,carGPS.lon,carGPS.lat)
      //println("当前点位状态"+location._1+","+location._2)
      val locationGPS=location._1
      val locationType=location._2
      //路线周期开始
      //println("路线周期开始")
      jedis.hset("muck:" + carGPS.vehicleNo + "_" + carGPS.vehicleColor + "_" + permit_license_no, "routeGPS",locationGPS)
      jedis.hset("muck:" + carGPS.vehicleNo + "_" + carGPS.vehicleColor + "_" + permit_license_no, "routeWay",locationType)
      jedis.hset("muck:" + carGPS.vehicleNo + "_" + carGPS.vehicleColor + "_" + permit_license_no, "routeTime",carGPS.dateTime)

    }else if (routeWay !=null) {
      //println("车辆非第一次次准运证路线记录")
      val routeWayArr = routeWay.split(";")
      val routTimeArr = routetime.split(";")
      val startType = routeWayArr(0)
      val startTime = routTimeArr(0)
      val location = getLocationType(routePointArr, carGPS.lon, carGPS.lat)
      //println("当前点位+"+carGPS.lon, carGPS.lat+"+状态"+location._1+","+location._2)

      val locationGPS = location._1
      val locationType = location._2

      locationType match {
        case "0" => { //当前定位为未知点时
          //println("当前点位未知点")
          if ((startType == "1" || startType == "3") && routeWayArr.length == 1) { //如果第一次的定位是起点或终点，则只记录一次未知点，考虑到出去又回来的情况
            routeGPS = routeGPS + ";" + locationGPS
            routeWay = routeWay + ";" + locationType
            routetime = routetime + ";" + carGPS.dateTime
            //println("开始点为起点或终点，记录一次未知点")
            jedis.hset("muck:" + carGPS.vehicleNo + "_" + carGPS.vehicleColor + "_" + permit_license_no, "routeGPS", routeGPS)
            jedis.hset("muck:" + carGPS.vehicleNo + "_" + carGPS.vehicleColor + "_" + permit_license_no, "routeWay", routeWay)
            jedis.hset("muck:" + carGPS.vehicleNo + "_" + carGPS.vehicleColor + "_" + permit_license_no, "routeTime", routetime)

          }
        }
        case "1" => { //当前定位为起点时
          //println("当前点位起点")

          if (startType == "1") { //如果第一次记录为起点
            if (routeWayArr.length > 1) { //中间经过了非起点的位置（出去了又回来了）
              //println("起点和起点之间记录了其他点位")
              //判断不按准运证规定时间生成隐患
              processPermitTime(kafkaProducer, passTimeArr, carGPS, industry, typeCode, startTime, carGPS.dateTime,permit_license_no,hiddenRuleMap)
            }
          } else {
            //判断不按准运证规定时间生成隐患
            processPermitTime(kafkaProducer, passTimeArr, carGPS, industry, typeCode, startTime, carGPS.dateTime,permit_license_no,hiddenRuleMap)
          }
          //println("路线周期结束")
          //路线周期结束
          jedis.hset("muck:" + carGPS.vehicleNo + "_" + carGPS.vehicleColor + "_" + permit_license_no, "routeGPS", locationGPS)
          jedis.hset("muck:" + carGPS.vehicleNo + "_" + carGPS.vehicleColor + "_" + permit_license_no, "routeWay", locationType)
          jedis.hset("muck:" + carGPS.vehicleNo + "_" + carGPS.vehicleColor + "_" + permit_license_no, "routeTime", carGPS.dateTime)

        }

        case "2" => { //当前定位为途经点时
          //println("当前点为途经点")
          val routePoints = routeGPS.split(";")
          var lastPointGps="-1,-1"
          if (routePoints.length>0){
            lastPointGps=routePoints(routePoints.length-1)
          }
          if (lastPointGps !="-1,-1"&&lastPointGps==locationGPS){
            var routetimeArr = routetime.split(";")
            routetimeArr(routetimeArr.length-1)=carGPS.dateTime
          }else{
            routetime = routetime + ";" + carGPS.dateTime
            routeGPS = routeGPS + ";" + locationGPS
            routeWay = routeWay + ";" + locationType
          }
          jedis.hset("muck:" + carGPS.vehicleNo + "_" + carGPS.vehicleColor + "_" + permit_license_no, "routeGPS", routeGPS)
          jedis.hset("muck:" + carGPS.vehicleNo + "_" + carGPS.vehicleColor + "_" + permit_license_no, "routeWay", routeWay)
          jedis.hset("muck:" + carGPS.vehicleNo + "_" + carGPS.vehicleColor + "_" + permit_license_no, "routeTime", routetime)
          if (startType == "1") {
            var driverGPS = jedis.hget("muck:" + carGPS.vehicleNo + "_" + carGPS.vehicleColor + "_" + permit_license_no, "routeGPS")

            val driverGPSArr = driverGPS.split(";")
            val newDriverGPS = driverGPSArr.filter(_ != "-1,-1").mkString(";")
            //println("本次路段"+newDriverGPS)
            if (!pass_route_location.contains(newDriverGPS)) { //本次路段不在规定点位
              //处理不按准运证规定路线生成隐患
              processPermitRouteProduce(kafkaProducer, pass_route_location, carGPS, industry, typeCode, newDriverGPS,startTime,carGPS.dateTime,permit_license_no,hiddenRuleMap)
              //判断不按准运证规定时间生成隐患
              processPermitTime(kafkaProducer, passTimeArr, carGPS, industry, typeCode, startTime, carGPS.dateTime,permit_license_no,hiddenRuleMap)
            }
          }
        }
        case "3" => { //当前点位为终点时
          //println("当前点为终点")

          routeGPS = routeGPS + ";" + locationGPS
          routeWay = routeWay + ";" + locationType
          routetime = routetime + ";" + carGPS.dateTime
          jedis.hset("muck:" + carGPS.vehicleNo + "_" + carGPS.vehicleColor + "_" + permit_license_no, "routeGPS", routeGPS)
          jedis.hset("muck:" + carGPS.vehicleNo + "_" + carGPS.vehicleColor + "_" + permit_license_no, "routeWay", routeWay)
          jedis.hset("muck:" + carGPS.vehicleNo + "_" + carGPS.vehicleColor + "_" + permit_license_no, "routeTime", routetime)
          if (startType == "1") { //如果第一次记录为起点
            //println("第一次为起点")
            var driverGPS = jedis.hget("muck:" + carGPS.vehicleNo + "_" + carGPS.vehicleColor + "_" + permit_license_no, "routeGPS")
            val driverGPSArr = driverGPS.split(";")
            val newDriverGPS = driverGPSArr.filter(_ != "-1,-1").mkString(";")
            //println("本次路线"+newDriverGPS)
            if (!pass_route_location.contains(newDriverGPS)) { //本次路线不在规定点位
              //处理不按准运证规定路线生成隐患
              processPermitRouteProduce(kafkaProducer, pass_route_location, carGPS, industry, typeCode, newDriverGPS,startTime,carGPS.dateTime,permit_license_no,hiddenRuleMap)
            }
            //判断不按准运证规定时间生成隐患
            processPermitTime(kafkaProducer, passTimeArr, carGPS, industry, typeCode, startTime, carGPS.dateTime,permit_license_no,hiddenRuleMap)

          }else if (startType == "3"){
            if (routeWayArr.length > 1) { //中间经过了非终点的位置（出去了又回来了）
              //println("终点和终点之间记录了其他点位")

              //判断不按准运证规定时间生成隐患
              processPermitTime(kafkaProducer, passTimeArr, carGPS, industry, typeCode, startTime, carGPS.dateTime,permit_license_no,hiddenRuleMap)
            }
          }
          //println("路线周期结束")
          //路线周期结束
          jedis.hset("muck:" + carGPS.vehicleNo + "_" + carGPS.vehicleColor + "_" + permit_license_no, "routeGPS", locationGPS)
          jedis.hset("muck:" + carGPS.vehicleNo + "_" + carGPS.vehicleColor + "_" + permit_license_no, "routeWay", locationType)
          jedis.hset("muck:" + carGPS.vehicleNo + "_" + carGPS.vehicleColor + "_" + permit_license_no, "routeTime", carGPS.dateTime)
        }
      }
    }
  }


  /**
   * 通行证未按规定路线
   *
   * @param carGPS
   * @param jedis
   * @param typeCode
   * @return
   */
  def processPassRoute(kafkaProducer: KafkaProducer[String, String],carGPS: CarGPS, jedis: Jedis,industry: String, typeCode: String,pass_time_interval: String,pass_route_location: String,hiddenRuleMap: Map[String, Boolean]) = {
    //println("进入通行证路线周期判断")
    val addr: (String, String, String, String) = GPSUtil.getAddressAndRoad(carGPS.lon.toDouble,carGPS.lat.toDouble)
    //当前定位地址详细名称
    val addrName=addr._1

    val passTimeArr = pass_time_interval.split(",")
    var routeWay = jedis.hget("pass:" + carGPS.vehicleNo + "_" + carGPS.vehicleColor, "routeWay")
    var routetime = jedis.hget("pass:" + carGPS.vehicleNo + "_" + carGPS.vehicleColor, "routeTime")
    var routeName = jedis.hget("pass:" +    carGPS.vehicleNo + "_" + carGPS.vehicleColor, "routeName")

    val routePointArr = pass_route_location.split("\\|")
    if (routeWay==null){
      //println("车辆第一次通行证路线记录"+pass_route_location)
      val location=getAddrNameType(routePointArr,addrName)
      //println("当前点位状态"+location._1+","+location._2)
      val locationName=location._1
      val locationType=location._2
      //路线周期开始
      //println("路线周期开始")
      jedis.hset("pass:" + carGPS.vehicleNo + "_" + carGPS.vehicleColor, "routeName",locationName)
      jedis.hset("pass:" + carGPS.vehicleNo + "_" + carGPS.vehicleColor, "routeWay",locationType)
      jedis.hset("pass:" + carGPS.vehicleNo + "_" + carGPS.vehicleColor, "routeTime",carGPS.dateTime)

    }else if (routeWay !=null) {
      //println("车辆非第一次次通行证路线记录")
      val routeWayArr = routeWay.split(";")
      val routTimeArr = routetime.split(";")
      val startType = routeWayArr(0)
      val startTime = routTimeArr(0)
      val location = getAddrNameType(routePointArr, addrName)
      //println("当前点位状态"+location._1+","+location._2)

      val locationName = location._1
      val locationType = location._2

      locationType match {
        case "0" => { //当前定位为未知点时
          //println("当前点位未知点")
          if ((startType == "1" || startType == "3") && routeWayArr.length == 1) { //如果第一次的定位是起点或终点，则只记录一次未知点，考虑到出去又回来的情况
            routeName = routeName + ";" + locationName
            routeWay = routeWay + ";" + locationType
            routetime = routetime + ";" + carGPS.dateTime
            //println("开始点为起点或终点，记录一次未知点")
            jedis.hset("pass:" + carGPS.vehicleNo + "_" + carGPS.vehicleColor, "routeName", routeName)
            jedis.hset("pass:" + carGPS.vehicleNo + "_" + carGPS.vehicleColor, "routeWay", routeWay)
            jedis.hset("pass:" + carGPS.vehicleNo + "_" + carGPS.vehicleColor, "routeTime", routetime)

          }
        }
        case "1" => { //当前定位为起点时
          //println("当前点位起点")

          if (startType == "1") { //如果第一次记录为起点
            if (routeWayArr.length > 1) { //中间经过了非起点的位置（出去了又回来了）
              //println("起点和起点之间记录了其他点位")
              //判断不按通行证规定时间生成隐患
              processPassTimeProduce(kafkaProducer, passTimeArr, carGPS, industry, typeCode, startTime, carGPS.dateTime,hiddenRuleMap)
            }
          } else {
            //判断不按准运证规定时间生成隐患
            processPassTimeProduce(kafkaProducer, passTimeArr, carGPS, industry, typeCode, startTime, carGPS.dateTime,hiddenRuleMap)
          }
          //println("路线周期结束")
          //路线周期结束
          jedis.hset("pass:" + carGPS.vehicleNo + "_" + carGPS.vehicleColor, "routeName", locationName)
          jedis.hset("pass:" + carGPS.vehicleNo + "_" + carGPS.vehicleColor, "routeWay", locationType)
          jedis.hset("pass:" + carGPS.vehicleNo + "_" + carGPS.vehicleColor, "routeTime", carGPS.dateTime)

        }

        case "2" => { //当前定位为途经点时
          //println("当前点为途经点")
          val routePoints = routeName.split(";")
          var lastPointName="-1,-1"
          if (routePoints.length>0){
            lastPointName=routePoints(routePoints.length-1)
          }
          if (lastPointName !="-1,-1"&&lastPointName==locationName){
            var routetimeArr = routetime.split(";")
            routetimeArr(routetimeArr.length-1)=carGPS.dateTime
          }else{
            routetime = routetime + ";" + carGPS.dateTime
            routeName = routeName + ";" + locationName
            routeWay = routeWay + ";" + locationType
          }
          jedis.hset("pass:" + carGPS.vehicleNo + "_" + carGPS.vehicleColor, "routeName", routeName)
          jedis.hset("pass:" + carGPS.vehicleNo + "_" + carGPS.vehicleColor, "routeWay", routeWay)
          jedis.hset("pass:" + carGPS.vehicleNo + "_" + carGPS.vehicleColor, "routeTime", routetime)
          if (startType == "1") {
            var driverName = jedis.hget("pass:" + carGPS.vehicleNo + "_" + carGPS.vehicleColor, "routeName")

            val driverNameArr = driverName.split(";")
            val newDriverName = driverNameArr.filter(_ != "-1,-1").mkString(";")
            //println("本次路段"+newDriverName)
            if (!pass_route_location.contains(newDriverName)) { //本次路段不在规定点位
              //处理不按通行证规定路线生成隐患
              processPassRouteProduce(kafkaProducer, pass_route_location, carGPS, industry, typeCode, newDriverName,startTime,carGPS.dateTime,hiddenRuleMap)
              //判断不按通行证规定时间生成隐患
              processPassTimeProduce(kafkaProducer, passTimeArr, carGPS, industry, typeCode, startTime, carGPS.dateTime,hiddenRuleMap)
            }
          }
        }
        case "3" => { //当前点位为终点时
          //println("当前点为终点")

          routeName = routeName + ";" + locationName
          routeWay = routeWay + ";" + locationType
          routetime = routetime + ";" + carGPS.dateTime
          jedis.hset("pass:" + carGPS.vehicleNo + "_" + carGPS.vehicleColor, "routeName", routeName)
          jedis.hset("pass:" + carGPS.vehicleNo + "_" + carGPS.vehicleColor, "routeWay", routeWay)
          jedis.hset("pass:" + carGPS.vehicleNo + "_" + carGPS.vehicleColor, "routeTime", routetime)
          if (startType == "1") { //如果第一次记录为起点
            //println("第一次为起点")
            var driverName = jedis.hget("pass:" + carGPS.vehicleNo + "_" + carGPS.vehicleColor, "routeName")
            val driverNameArr = driverName.split(";")
            val newDriverName = driverNameArr.filter(_ != "-1,-1").mkString(";")
            //println("本次路线"+newDriverName)
            if (!pass_route_location.contains(newDriverName)) { //本次路线不在规定点位
              //处理不按准运证规定路线生成隐患
              processPassRouteProduce(kafkaProducer, pass_route_location, carGPS, industry, typeCode, newDriverName,startTime,carGPS.dateTime,hiddenRuleMap)
            }
            //判断不按准运证规定时间生成隐患
            processPassTimeProduce(kafkaProducer, passTimeArr, carGPS, industry, typeCode, startTime, carGPS.dateTime,hiddenRuleMap)

          }else if (startType == "3"){
            if (routeWayArr.length > 1) { //中间经过了非终点的位置（出去了又回来了）
              //println("终点和终点之间记录了其他点位")

              //判断不按准运证规定时间生成隐患
              processPassTimeProduce(kafkaProducer, passTimeArr, carGPS, industry, typeCode, startTime, carGPS.dateTime,hiddenRuleMap)
            }
          }
          //println("路线周期结束")
          //路线周期结束
          jedis.hset("pass:" + carGPS.vehicleNo + "_" + carGPS.vehicleColor, "routeName", locationName)
          jedis.hset("pass:" + carGPS.vehicleNo + "_" + carGPS.vehicleColor, "routeWay", locationType)
          jedis.hset("pass:" + carGPS.vehicleNo + "_" + carGPS.vehicleColor, "routeTime", carGPS.dateTime)
        }
      }
    }
  }


  /**
   * 包车牌未按规定路线
   *
   * @param carGPS
   * @param jedis
   * @param typeCode
   * @return
   */
  def processCharterRoute(kafkaProducer: KafkaProducer[String, String],carGPS: CarGPS, jedis: Jedis,industry: String,
                          typeCode: String,start_point_name:String, alignments:String,aim_name:String,
                          grade_charter_mark:String,charter_start_date:String,charter_end_date:String,
                          is_return_passenger:String,hiddenRuleMap: Map[String, Boolean]) = {
    val addr: (String, String, String, String) = GPSUtil.getAddressAndRoad(carGPS.lon.toDouble,carGPS.lat.toDouble)
    //println(carGPS.lon+","+carGPS.lat)
    //println(carGPS.lon.toDouble+","+carGPS.lat.toDouble)
    //当前定位地址详细名称
    val addrName=addr._1

    val vehicleColorMap2 = Map[String, String]("蓝色" -> "1", "黄色" -> "2", "黑色" -> "3", "白色" -> "4","绿色" -> "5", "其他" -> "9") //车牌颜色map

    val charterId = carGPS.vehicleNo + "_" + vehicleColorMap2.getOrElse(carGPS.vehicleColor,"unknown")  + "_" + grade_charter_mark + "_" + charter_start_date + "_" + charter_end_date


    //println("进入包车牌路线周期判断")
    var routeWay = jedis.hget("charter:" + charterId, "routeWay")
    var routeName = jedis.hget("charter:" + charterId, "routeName")
    var routetime = jedis.hget("charter:" + charterId, "routeTime")
    var routeBackStatus = jedis.hget("charter:" + charterId, "routeBackStatus")


    if (routeWay==null){
      //println("车辆第一次包车路线记录"+start_point_name+";"+alignments+";"+aim_name)
      val location=getCharterLocationType(addrName,start_point_name,alignments,aim_name)
      //println("当前点位状态"+location._1+","+location._2)
      val locationName=location._1
      val locationType=location._2
      //路线周期开始
      //println("路线周期开始")
      jedis.hset("charter:" + charterId, "routeName",locationName)
      jedis.hset("charter:" + charterId, "routeWay",locationType)
      jedis.hset("charter:" + charterId, "routeTime",carGPS.dateTime)
      jedis.hset("charter:" + charterId, "typeCode",typeCode)
      jedis.hset("charter:" + charterId, "enterpriseName",carGPS.enterpriseName)
      jedis.hset("charter:" + charterId, "enterpriseCode",carGPS.dealDeptCode)

      if (locationType=="1"&&is_return_passenger=="1"&&routeBackStatus==null){//记录一次表示从起点出发
        jedis.hset("charter:" + charterId, "routeBackStatus","0")
      }

    }else if (routeWay !=null) {
      //println("车辆非第一次次包车牌路线记录")
      val routeWayArr = routeWay.split(";")
      val routTimeArr = routetime.split(";")
      val startType = routeWayArr(0)
      val startTime = routTimeArr(0)
      val location=getCharterLocationType(addrName,start_point_name,alignments,aim_name)
      //println("当前点位状态"+location._1+","+location._2)

      val locationName = location._1
      val locationType = location._2

      locationType match {
        case "0" => { //当前定位为未知点时
          //println("当前点位未知点")
        }
        case "1" => { //当前定位为起点时
          //println("当前点位起点")
          if (is_return_passenger=="1"&&routeBackStatus==null){//记录一次表示从起点出发
            jedis.hset("charter:" + charterId, "routeBackStatus","0")
          }else if (is_return_passenger=="1"&&routeBackStatus=="1"){//记录表示 从起点到非起点再到达起点
            jedis.hset("charter:" + charterId, "routeBackStatus","2")
            jedis.hset("charter:" + charterId, "routeBackTime",carGPS.dateTime)

          }

          if (startType == "1") { //如果第一次记录为起点

          } else {
            //println("路线周期结束")
            jedis.hset("charter:" + charterId, "routeName", locationName)
            jedis.hset("charter:" + charterId, "routeWay", locationType)
            jedis.hset("charter:" + charterId, "routeTime", carGPS.dateTime)

          }
          //路线周期结束

        }

        case "2" => { //当前定位为途经点时
          //println("当前点为途经点")
          val routePoints = routeName.split(";")
          var lastPointName="未知点"
          if (routePoints.length>0){
            lastPointName=routePoints(routePoints.length-1)
          }
          if (lastPointName !="未知点"&&lastPointName==locationName){

          }else{
            routeName = routeName + ";" + locationName
            routeWay = routeWay + ";" + locationType
            routetime = routetime + ";" + carGPS.dateTime

          }
          jedis.hset("charter:" + charterId, "routeName", routeName)
          jedis.hset("charter:" + charterId, "routeWay", routeWay)
          jedis.hset("charter:" + charterId, "routeTime", routetime)

        }
        case "3" => { //当前点位为终点时
          //println("当前点为终点")

          routeName = routeName + ";" + locationName
          routeWay = routeWay + ";" + locationType
          routetime = routetime + ";" + carGPS.dateTime

          jedis.hset("charter:" + charterId, "routeName", routeName)
          jedis.hset("charter:" + charterId, "routeWay", routeWay)
          jedis.hset("charter:" + charterId, "routeTime", routetime)

          if (startType == "1") { //如果第一次记录为起点
            //println("第一次为起点")
            if (is_return_passenger=="1"&&routeBackStatus=="0"){//记录一次表示从起点到达非起点状态
              jedis.hset("charter:" + charterId, "routeBackStatus","1")
            }
            var driverName = jedis.hget("charter:" + charterId, "routeName")
            val driverGPSArr = driverName.split(";")

            val RouteLength = alignments.split("[^\\u4e00-\\u9fa5a-zA-Z0-9（）]").length+2

            //println("本次路线"+driverName)
            //println(driverGPSArr.length)
            //println(RouteLength)
            if (driverGPSArr.length != RouteLength) { //本次路线不在规定点位
              //处理不按准运证规定路线生成隐患
              produceCharterRouteProduce(kafkaProducer, alignments, carGPS, industry, typeCode, driverName,startTime,carGPS.dateTime,hiddenRuleMap,charterId)
            }else{  //一次完整路线
              //连续5次定点定线运营
              if (startTime>=charter_start_date&&carGPS.dateTime.substring(0,10)<=charter_end_date){//本次路线的开始时间和结束时间在规定时间内
                val sameFullRoute=start_point_name+"_"+alignments+"_"+aim_name
                var sameRouteCount = jedis.hget("charter:" + carGPS.vehicleNo+"_"+carGPS.vehicleColor+"_"+sameFullRoute, "sameRouteCount")
                var sameRouteCharterId = jedis.hget("charter:" + carGPS.vehicleNo+"_"+carGPS.vehicleColor+"_"+sameFullRoute, "sameRouteCharterId")
                var sameRouteFullTime = jedis.hget("charter:" + carGPS.vehicleNo+"_"+carGPS.vehicleColor+"_"+sameFullRoute, "sameRouteFullTime")
                var sameRouteFullCharterId = jedis.hget("charter:" + carGPS.vehicleNo+"_"+carGPS.vehicleColor+"_"+sameFullRoute, "sameRouteFullCharterId")


                if (sameRouteCount == null) {
                  sameRouteCount = "1"
                  sameRouteCharterId = charterId
                  sameRouteFullTime = startTime + "_" + carGPS.dateTime
                  sameRouteFullCharterId=charterId
                  jedis.hset("charter:" + carGPS.vehicleNo + "_" + carGPS.vehicleColor + "_" + sameFullRoute, "sameRouteCount", sameRouteCount)
                  jedis.hset("charter:" + carGPS.vehicleNo + "_" + carGPS.vehicleColor + "_" + sameFullRoute, "sameRouteCharterId", sameRouteCharterId)
                  jedis.hset("charter:" + carGPS.vehicleNo + "_" + carGPS.vehicleColor + "_" + sameFullRoute, "sameRouteFullTime", sameRouteFullTime)
                  jedis.hset("charter:" + carGPS.vehicleNo + "_" + carGPS.vehicleColor + "_" + sameFullRoute, "sameRouteFullCharterId", sameRouteFullCharterId)

                } else if (sameRouteCharterId != charterId) {
                  if (sameRouteCount <= "4") {
                    sameRouteCount = (sameRouteCount.toInt + 1).toString
                    sameRouteCharterId = charterId
                    sameRouteFullTime += ";" + startTime + "_" + carGPS.dateTime
                    sameRouteFullCharterId+="|"+charterId
                    jedis.hset("charter:" + carGPS.vehicleNo + "_" + carGPS.vehicleColor + "_" + sameFullRoute, "sameRouteCount", sameRouteCount)
                    jedis.hset("charter:" + carGPS.vehicleNo + "_" + carGPS.vehicleColor + "_" + sameFullRoute, "sameRouteCharterId", sameRouteCharterId)
                    jedis.hset("charter:" + carGPS.vehicleNo + "_" + carGPS.vehicleColor + "_" + sameFullRoute, "sameRouteFullTime", sameRouteFullTime)
                    jedis.hset("charter:" + carGPS.vehicleNo + "_" + carGPS.vehicleColor + "_" + sameFullRoute, "sameRouteFullCharterId", sameRouteFullCharterId)

                    if (sameRouteCount == "5") {
                      val routeId=carGPS.vehicleNo + "_" + carGPS.vehicleColor + "_" + sameFullRoute
                      var detailArr=ArrayBuffer[String]()
                      //println("处理连续5次定点定线运营隐患")

                      val array = ArrayBuffer[HiddenTrajectory]()
                      val vehiclePlateColor=vehicleColorMap2.getOrElse(carGPS.vehicleColor, "unknown")
                      val routeTimeArr = sameRouteFullTime.split(";")
                      val routeFullCharterIdArr = sameRouteFullCharterId.split("\\|")

                      var i: Int=0
                      routeTimeArr.map(fullTime=>{
                        i=i+1
                        val fullTimeArr = fullTime.split("_")
                        val driverStartTime=fullTimeArr(0)
                        val driverEndTime=fullTimeArr(1)
                        detailArr.append("车辆于"+driverStartTime+"开始第"+i.toString+"次运营")
                        array+=HiddenTrajectory(i.toString,carGPS.vehicleNo,vehiclePlateColor,driverStartTime,driverEndTime,routeFullCharterIdArr(i-1))
                      })
                      val hiddenEvidence: HiddenEvidence = HiddenEvidence(
                        carGPS.dateTime,
                        carGPS.dateTime,
                        routeId,
                        array.toArray
                      )
                      var index ="vehicle_line_trajectory_index"
                      val typeStr: String = index.replace("index", "type")
                      val jestClient: JestClient = EsUtils.getClient()
                      //将隐患证据写入es
                      EsUtils.insertIntoEs(jestClient, "vehicle_line_trajectory_index", typeStr, hiddenEvidence)

                      produceCharterSameRouteNumberProduce(kafkaProducer,carGPS,industry,typeCode,routeTimeArr,hiddenRuleMap,routeId,detailArr.toArray)

                    }
                  }
                }
              }

              //未按规定趟次
              var routeCount = jedis.hget("charter:" + charterId, "routeCount")
              var routeFullTime = jedis.hget("charter:" + charterId, "routeFullTime")

              if (routeCount==null){
                jedis.hset("charter:" + charterId, "routeCount","1")
                jedis.hset("charter:" + charterId,"routeFullTime", startTime+"_"+carGPS.dateTime)

              }else if (routeCount=="1"){
                jedis.hset("charter:" + charterId, "routeCount","2")
                jedis.hset("charter:" + charterId,"routeFullTime", routeFullTime+";"+startTime+"_"+carGPS.dateTime)

                val array = ArrayBuffer[HiddenTrajectory]()
                val vehiclePlateColor=vehicleColorMap2.getOrElse(carGPS.vehicleColor, "unknown")
                val routeTimeArr = routeFullTime.split("_")
                //println(charter_start_date+charter_end_date)
                if (routeTimeArr(0).substring(0,10)>=charter_start_date&&carGPS.dateTime.substring(0,10)<=charter_end_date){//当第一次路线的开始时间和第二次路线的结束时间在规定时间内
                  array+=HiddenTrajectory(1.toString,carGPS.vehicleNo,vehiclePlateColor,routeTimeArr(0),routeTimeArr(1),null)
                  array+=HiddenTrajectory(2.toString,carGPS.vehicleNo,vehiclePlateColor,startTime,carGPS.dateTime,null)
                  val hiddenEvidence: HiddenEvidence = HiddenEvidence(
                    carGPS.dateTime,
                    carGPS.dateTime,
                    charterId,
                    array.toArray
                  )
                  var index ="vehicle_line_trajectory_index"
                  val typeStr: String = index.replace("index", "type")
                  val jestClient: JestClient = EsUtils.getClient()
                  //将隐患证据写入es
                  EsUtils.insertIntoEs(jestClient, "vehicle_line_trajectory_index", typeStr, hiddenEvidence)

                  produceCharterRouteNumberProduce(kafkaProducer,carGPS,industry,typeCode,routeTimeArr(1),carGPS.dateTime,hiddenRuleMap,charterId)
                }
              }
            }
          }
          ////println("路线周期结束")
          //路线周期结束
          jedis.hset("charter:" + charterId, "routeName", locationName)
          jedis.hset("charter:" + charterId, "routeWay", locationType)
          jedis.hset("charter:" + charterId, "routeTime", carGPS.dateTime)
        }
      }
    }
  }


  /**
   * 判断不按准运证规定时间行驶
   * @param passTimeArr
   * @param carGPS
   * @param typeCode
   * @return
   */
  def processPermitTime(kafkaProducer: KafkaProducer[String, String],passTimeArr: Array[String], carGPS: CarGPS, industry: String,typeCode: String,startTime:String,endTime:String,permit_license_no:String,hiddenRuleMap: Map[String, Boolean]) = {
    ////println("处理不按准运证规定时间行驶")
    //println(startTime)
    //println(endTime)
    val startDayOfWeek = Tools.getDayOfWeek(startTime)
    val endDayOfWeek = Tools.getDayOfWeek(endTime)
    var DriverStartTime = "00:00"
    var DriverEndTime = "00:00"
    if (startTime.length>16){
      DriverStartTime=startTime.substring(11,16)
    }
    if (startTime.length>16){
      DriverEndTime=endTime.substring(11,16)
    }
    //2-0-08:00-08:00,1-1-08:00-10:01,1-1-11:00-12:00

    var isInRouteTime=false
    passTimeArr.foreach(passTime=>{
      val passArray = passTime.split("\\-")
      val passLength=passArray.length
      if (passLength>1){
        val weekType=passArray(0) //0 每日，1 周一到周五，2 周六周日，3 工作日，4 节假日
        val workType=passArray(1) //0 全天，1 定时

        val arrayBuffer = new ArrayBuffer[Int]()
        weekType match {
          case "0" => arrayBuffer+=(1,2,3,4,5,6,7)
          case "1" => arrayBuffer+=(1,2,3,4,5)
          case "2" => arrayBuffer+=(6,7)
          case "3" => arrayBuffer+=(1,2,3,4,5)
          case _ =>
        }
        if (arrayBuffer.contains(startDayOfWeek)&&arrayBuffer.contains(endDayOfWeek)){
          if (workType=="0"){
            isInRouteTime=true
          }else if (workType=="1"&&passLength==4){
            val routeStartTime=passArray(2)
            val routeEndTime=passArray(3)
            if (DriverStartTime < DriverEndTime) { //当没有规定时间跨天
              if (DriverStartTime >= routeStartTime && DriverEndTime <= routeEndTime) {

                isInRouteTime = true
              }
            } else { //规定时间跨天了
              if (DriverStartTime >= routeStartTime && "23:59" <=  routeEndTime|| "00:00" >= routeStartTime && DriverEndTime <= routeEndTime) {
                isInRouteTime = true
              }
            }
          }
        }
      }
    })
    //println("isInrouteTime:"+isInRouteTime)
    if (!isInRouteTime) {
      // 隐患默认发往哪个topic
      val transport_supervise_topic = "transport_supervise_topic"
      val hiddenName = "不按准运证规定时间行驶"
      val superviseConfigId = "2"
      val superviseRuleConfigId = "2"
      val content = carGPS.vehicleNo + "_" + carGPS.dateTime + "_产生隐患：" + hiddenName + ",规定时间数据为：" + passTimeArr.mkString(";")
      val generateObject = carGPS.vehicleNo
      val generateObjectCode = permit_license_no
      val generate_detail = carGPS.vehicleNo + "于" + carGPS.dateTime + "营运"

      processRouteHidden(hiddenName, carGPS, industry, kafkaProducer, typeCode, content, superviseConfigId, superviseRuleConfigId, generateObject, generateObjectCode, transport_supervise_topic, generate_detail, startTime, endTime, hiddenRuleMap)
    }
  }

  /**
   * 判断不按准运证规定时间行驶
   * @param passTimeArr
   * @param carGPS
   * @param typeCode
   * @return
   */
  def processPassTimeProduce(kafkaProducer: KafkaProducer[String, String],passTimeArr: Array[String], carGPS: CarGPS, industry: String,typeCode: String,startTime:String,endTime:String,hiddenRuleMap: Map[String, Boolean]) = {
    //println("处理不按通行证规定时间行驶")
    //println(startTime)
    //println(endTime)
    val startDayOfWeek = Tools.getDayOfWeek(startTime)
    val endDayOfWeek = Tools.getDayOfWeek(endTime)
    var DriverStartTime = "00:00"
    var DriverEndTime = "00:00"
    if (startTime.length>16){
      DriverStartTime=startTime.substring(11,16)
    }
    if (startTime.length>16){
      DriverEndTime=endTime.substring(11,16)
    }
    //2-0-08:00-08:00,1-1-08:00-10:01,1-1-11:00-12:00

    var isInRouteTime=false
    passTimeArr.foreach(passTime=>{
      val passArray = passTime.split("\\-")
      val passLength=passArray.length
      if (passLength>1){
        val weekType=passArray(0) //0 每日，1 周一到周五，2 周六周日，3 工作日，4 节假日
        val workType=passArray(1) //0 全天，1 定时

        val arrayBuffer = new ArrayBuffer[Int]()
        weekType match {
          case "0" => arrayBuffer+=(1,2,3,4,5,6,7)
          case "1" => arrayBuffer+=(1,2,3,4,5)
          case "2" => arrayBuffer+=(6,7)
          case "3" => arrayBuffer+=(1,2,3,4,5)
          case _ =>
        }
        if (arrayBuffer.contains(startDayOfWeek)&&arrayBuffer.contains(endDayOfWeek)){
          if (workType=="0"){
            isInRouteTime=true
          }else if (workType=="1"&&passLength==4){
            val routeStartTime=passArray(2)
            val routeEndTime=passArray(3)
              if (DriverStartTime < DriverEndTime) { //当没有规定时间跨天
                if (DriverStartTime >= routeStartTime && DriverEndTime <= routeEndTime) {

                  isInRouteTime = true
                }
              } else { //规定时间跨天了
                if (DriverStartTime >= routeStartTime && "23:59" <=  routeEndTime|| "00:00" >= routeStartTime && DriverEndTime <= routeEndTime) {
                  isInRouteTime = true
                }
              }
          }
        }
      }
    })
    //println("isInrouteTime:"+isInRouteTime)
    if (!isInRouteTime) {
      // 隐患默认发往哪个topic
      val transport_supervise_topic = "transport_supervise_topic"
      val hiddenName = "不按通行证规定时间行驶"
      val superviseConfigId = "3"
      val superviseRuleConfigId = "3"
      val content = carGPS.vehicleNo + "_" + carGPS.dateTime + "_产生隐患：" + hiddenName + ",规定时间数据为：" + passTimeArr.mkString(";")
      val generateObject = carGPS.vehicleNo
      val generateObjectCode = carGPS.vehicleNo
      val generate_detail = carGPS.vehicleNo + "于" + carGPS.dateTime + "营运"

      processRouteHidden(hiddenName, carGPS, industry, kafkaProducer, typeCode, content, superviseConfigId, superviseRuleConfigId, generateObject, generateObjectCode, transport_supervise_topic, generate_detail, startTime, endTime, hiddenRuleMap)
    }
  }

  def processPermitRouteProduce(kafkaProducer: KafkaProducer[String, String], pass_route_location: String, carGPS: CarGPS, industry: String, typeCode: String, newDriverGPS: String,startTime:String,endTime:String,permit_license_no:String,hiddenRuleMap: Map[String, Boolean]) = {
    //println("处理不按准运证规定路线行驶")
    // 隐患默认发往哪个topic
    val transport_supervise_topic="transport_supervise_topic"
    val hiddenName="不按准运证规定路线行驶"
    val superviseConfigId="4"
    val superviseRuleConfigId="4"
    val content=carGPS.vehicleNo + "_" + carGPS.dateTime + "_产生隐患：" + hiddenName + ",规定路线数据为：" + pass_route_location+"，当前路线为"+newDriverGPS
    val generateObject=carGPS.vehicleNo
    val generateObjectCode=permit_license_no
    val generate_detail=carGPS.vehicleNo+"于"+carGPS.dateTime+"不按准运证规定线路行驶"
    //println(generateObjectCode)

    processRouteHidden(hiddenName, carGPS, industry, kafkaProducer, typeCode, content,  superviseConfigId, superviseRuleConfigId,generateObject,generateObjectCode,transport_supervise_topic,generate_detail,startTime, endTime,hiddenRuleMap)

  }

  def processPassRouteProduce(kafkaProducer: KafkaProducer[String, String], pass_route_location: String, carGPS: CarGPS, industry: String, typeCode: String, newDriverGPS: String,startTime:String,endTime:String,hiddenRuleMap: Map[String, Boolean]) = {
    //println("处理不按通行证规定路线行驶")
    // 隐患默认发往哪个topic
    val transport_supervise_topic="transport_supervise_topic"
    val hiddenName="不按通行证规定路线行驶"
    val superviseConfigId="5"
    val superviseRuleConfigId="5"
    val content=carGPS.vehicleNo + "_" + carGPS.dateTime + "_产生隐患：" + hiddenName + ",规定路线数据为：" + pass_route_location+"，当前路线为"+newDriverGPS
    val generateObject=carGPS.vehicleNo
    val generateObjectCode=carGPS.vehicleNo
    val generate_detail=carGPS.vehicleNo+"于"+carGPS.dateTime+"不按通行证规定线路行驶"

    processRouteHidden(hiddenName, carGPS, industry, kafkaProducer, typeCode, content,  superviseConfigId, superviseRuleConfigId,generateObject,generateObjectCode,transport_supervise_topic,generate_detail,startTime, endTime,hiddenRuleMap)

  }

  def produceCharterRouteProduce(kafkaProducer: KafkaProducer[String, String], route_location: String, carGPS: CarGPS,
                          industry: String, typeCode: String, DriverGPS: String,startTime:String,endTime:String
                          ,hiddenRuleMap: Map[String, Boolean],generateObjectCode:String) = {
    //println("处理不按包车牌规定路线行驶")
    // 隐患默认发往哪个topic
    val transport_supervise_topic="transport_supervise_topic"
    val hiddenName="未按包车线路行驶线路行驶"
    val superviseConfigId="32"
    val superviseRuleConfigId="32"
    val content=carGPS.vehicleNo + "_" + carGPS.dateTime + "_产生隐患：" + hiddenName + ",规定路线途径：" + route_location+"，当前路线为"+DriverGPS
    val generateObject=carGPS.vehicleNo
    val generate_detail=carGPS.vehicleNo+"于"+carGPS.dateTime+"未按包车线路行驶线路行驶"

    processRouteHidden(hiddenName, carGPS, industry, kafkaProducer, typeCode, content,  superviseConfigId, superviseRuleConfigId,generateObject,generateObjectCode,transport_supervise_topic,generate_detail,startTime, endTime,hiddenRuleMap)

  }

  def produceCharterRouteNumberProduce(kafkaProducer: KafkaProducer[String, String], carGPS: CarGPS, industry: String, typeCode: String,fristEndTime:String,secondEndTime:String,hiddenRuleMap: Map[String, Boolean],generateObjectCode:String) = {
    //println("处理未按规定趟次进行运营")
    // 隐患默认发往哪个topic
    val transport_supervise_topic="transport_supervise_topic"
    val hiddenName="处理未按规定趟次进行运营"
    val superviseConfigId="33"
    val superviseRuleConfigId="33"
    val content="车辆于"+fristEndTime+"第1次到达终点;车辆于"+secondEndTime+"第2次到达终点"
    val generateObject=carGPS.vehicleNo
    val generate_detail=carGPS.vehicleNo+"于"+carGPS.dateTime+"处理未按规定趟次进行运营"

    processRouteHidden(hiddenName, carGPS, industry, kafkaProducer, typeCode, content,  superviseConfigId, superviseRuleConfigId,generateObject,generateObjectCode,transport_supervise_topic,generate_detail,null, null,hiddenRuleMap)

  }

  def produceCharterSameRouteNumberProduce(kafkaProducer: KafkaProducer[String, String], carGPS: CarGPS, industry: String, typeCode: String,routeTimeArr:Array[String],hiddenRuleMap: Map[String, Boolean],generateObjectCode:String,detailArr:Array[String]) = {
    //println("生成连续5次从A-B定点定线运营")
    // 隐患默认发往哪个topic
    val transport_supervise_topic="transport_supervise_topic"
    val hiddenName="连续5次从A-B定点定线运营"
    val superviseConfigId="35"
    val superviseRuleConfigId="35"
    val content=carGPS.vehicleNo+"于"+carGPS.dateTime+"生成连续5次从A-B定点定线运营隐患"
    val generateObject=carGPS.vehicleNo
    val generate_detail=detailArr.mkString(";")

    processRouteHidden(hiddenName, carGPS, industry, kafkaProducer, typeCode, content,  superviseConfigId, superviseRuleConfigId,generateObject,generateObjectCode,transport_supervise_topic,generate_detail,null, null,hiddenRuleMap)

  }

}
