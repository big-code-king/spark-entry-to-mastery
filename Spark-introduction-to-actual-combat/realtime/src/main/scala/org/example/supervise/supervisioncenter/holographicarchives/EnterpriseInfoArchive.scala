package org.example.supervise.supervisioncenter.holographicarchives

import org.apache.spark.broadcast.Broadcast
import org.apache.spark.sql.SparkSession
import org.example.client.DbClient
import org.example.common.{Logging, Sparking}
import org.example.constant.ApolloConst
import org.example.utils.Tools
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import scalikejdbc.{NamedDB, SQL}

import java.sql.Timestamp
import java.util.Properties

/**
 * 企业档案,推送到 zcov.yz_enterprise_info_archive
 */
object EnterpriseInfoArchive extends Sparking with Logging {
  def main(args: Array[String]): Unit = {
    val session = SparkSession.builder().config(conf)
      .config("hive.metastore.uris", ApolloConst.hiveMetastore)
      .enableHiveSupport()
      .getOrCreate()
    val todayStr: String = new DateTime().toString("yyyy-MM-dd")
    val yesterdayStr: String = DateTime.parse(todayStr, DateTimeFormat.forPattern("yyyy-MM-dd")).plusDays(-1).toString("yyyy-MM-dd")
    import session.implicits._
    val prop = new Properties()
    prop.put("user", ApolloConst.jgdMysqlUserName)
    prop.put("password", ApolloConst.jgdMysqlPassWord)
    prop.put("driver", ApolloConst.jgdMysqlDriver)
    //企业云同步到监管端的企业基本信息
    session.read.jdbc(ApolloConst.jgdMysqlURL, "zcov.basic_enterprise_info", prop).toDF().createOrReplaceTempView("base_into_enterprise_info")
    //经营范围字典数据获取，写入到Map中
    val scopeCodeNameMap: Map[String, String] = session.sql("select business_code,business_scope_name from dim.business_scope_dic").map { row =>
      val business_code: String = row.getAs[String]("business_code")
      val business_scope_name: String = row.getAs[String]("business_scope_name")
      (business_code, business_scope_name)
    }.collect().toMap
    val scopeCodeNameBroadcast: Broadcast[Map[String, String]] = session.sparkContext.broadcast(scopeCodeNameMap)

    DbClient.init("jdgMysql", ApolloConst.jgdMysqlDriver, ApolloConst.jgdMysqlURL, ApolloConst.jgdMysqlUserName, ApolloConst.jgdMysqlPassWord)
    DbClient.usingDB("jdgMysql") { db: NamedDB =>
      val sqlStr =
        s"""
           |truncate table zcov.yz_enterprise_info_archive
           |""".stripMargin
      db autoCommit { implicit session =>
        SQL(sqlStr).update().apply()
      }
    }
    //同步企业档案到监管端mysql：zcov.yz_enterprise_info_archive
    session.sql(
      """
        |SELECT
        |   t2.enterprise_name, -- 运政-企业名称
        |   t1.social_credit_code, --统一社会信用代码
        |   t1.business_owner_id enterprise_code, -- -- 运政-企业编码
        |   case when t1.business_owner_economic_category ='100' then '内资'
        |       when t1.business_owner_economic_category ='110' then '国有全资'
        |       when t1.business_owner_economic_category ='120' then '集体全资'
        |       when t1.business_owner_economic_category ='130' then '股份合作'
        |       when t1.business_owner_economic_category ='140' then '联营'
        |       when t1.business_owner_economic_category ='141' then '国有联营'
        |       when t1.business_owner_economic_category ='142' then '集体联营'
        |       when t1.business_owner_economic_category ='143' then '国有与集体联营'
        |       when t1.business_owner_economic_category ='149' then '其他联营'
        |       when t1.business_owner_economic_category ='150' then '有限责任（公司）'
        |       when t1.business_owner_economic_category ='151' then '国有独资（公司）'
        |       when t1.business_owner_economic_category ='159' then '其他有限责任（公司）'
        |       when t1.business_owner_economic_category ='160' then '股份有限（公司）'
        |       when t1.business_owner_economic_category ='170' then '私有'
        |       when t1.business_owner_economic_category ='171' then '私有独资'
        |       when t1.business_owner_economic_category ='172' then '私有合伙'
        |       when t1.business_owner_economic_category ='173' then '私营有限责任（公司）'
        |       when t1.business_owner_economic_category ='174' then '私营股份有限（公司）'
        |       when t1.business_owner_economic_category ='175' then '个体经营'
        |       when t1.business_owner_economic_category ='179' then '其他私有'
        |       when t1.business_owner_economic_category ='190' then '其他内资'
        |       when t1.business_owner_economic_category ='200' then '港、澳、台投资'
        |       when t1.business_owner_economic_category ='210' then '内地和港、澳、台合资'
        |       when t1.business_owner_economic_category ='220' then '内地和港、澳、台合作'
        |       when t1.business_owner_economic_category ='230' then '港、澳、台独资'
        |       when t1.business_owner_economic_category ='240' then '港、澳、台投资股份有限（公司）'
        |       when t1.business_owner_economic_category ='290' then '其他港、澳、台投资'
        |       when t1.business_owner_economic_category ='300' then '国外投资'
        |       when t1.business_owner_economic_category ='310' then '中外合资'
        |       when t1.business_owner_economic_category ='320' then '中外合作'
        |       when t1.business_owner_economic_category ='330' then '外资'
        |       when t1.business_owner_economic_category ='340' then '国外投资股份有限（公司）'
        |       when t1.business_owner_economic_category ='390' then '其他国外投资'
        |       when t1.business_owner_economic_category ='900' then '其他'
        |       else '' end business_owner_economic_category, --运政-经营业户经济类型
        |   t2.area, --运政-区地区编码
        |   t2.address, --运政-联系地址
        |   t2.administrative_organization_id, -- 运政-行政机构ID
        |	  case when t2.administrative_organization_id ='52010000' then '贵州省贵阳市'
        |        when t2.administrative_organization_id ='52010200' then '贵州省贵阳市南明区'
        |        when t2.administrative_organization_id ='52010300' then '贵州省贵阳市云岩区'
        |        when t2.administrative_organization_id ='52011100' then '贵州省贵阳市花溪区'
        |        when t2.administrative_organization_id ='52011200' then '贵州省贵阳市乌当区'
        |        when t2.administrative_organization_id ='52011300' then '贵州省贵阳市白云区'
        |        when t2.administrative_organization_id ='52011400' then '贵州省贵阳市小河区'
        |        when t2.administrative_organization_id ='52011500' then '贵州省贵阳市观山湖区'
        |        when t2.administrative_organization_id ='52012100' then '贵州省贵阳市开阳县'
        |        when t2.administrative_organization_id ='52012200' then '贵州省贵阳市息烽县'
        |        when t2.administrative_organization_id ='52012300' then '贵州省贵阳市修文县'
        |        when t2.administrative_organization_id ='52018100' then '贵州省贵阳市清镇市'
        |        when t2.administrative_organization_id ='' then '未知行政机构'
        |        else '' end administrative_organization_name, -- 运政-行政机构名称
        |	  t2.whether_branch, -- 运政-是否分支:1是
        |	  t2.business_owner_code, -- 运政-业户代码
        |   case when t2.business_owner_status = '1' then '营业'
        |          when t2.business_owner_status = '2' then '停业'
        |          when t2.business_owner_status = '3' then '整改'
        |          when t2.business_owner_status = '4' then '停业整顿'
        |          when t2.business_owner_status = '5' then '歇业'
        |          when t2.business_owner_status = '6' then '注销'
        |          when t2.business_owner_status = '9' then '其他'
        |          else '其他' end business_owner_status, -- 运政-业户状态
        |	  t2.phone_number, -- 运政-联系电话
        |	  t2.postal_code, -- 运政-邮政编码
        |	  t2.email, -- 运政-电子邮箱
        |	  t2.remark, -- 运政-备注
        |	  t2.department, -- 运政-所属部门
        |	  t2.fax_number, -- 运政-传真电话
        |	  t2.organization_of_created_file, -- 运政-建档机构
        |	  t2.create_date, -- 运政-创建日期
        |	  t2.operator_name, -- 运政-经办人姓名
        |	  t2.operator_phone_number, -- 运政-经办人电话
        |	  t2.name_of_the_person_in_charge, -- 运政-负责人姓名
        |
        |   t1.legal_representative, -- 运政工商信息-法定代表人
        |	  t1.legal_representative_number_of_id_certificate, -- 运政工商信息-法人身份证
        |	  t1.business_registration_number, -- 运政工商信息-工商注册号
        |	  case when t1.certificate_type_of_legal_representative ='1' then '居民身份证'
        |        when t1.certificate_type_of_legal_representative ='2' then '军官证'
        |        when t1.certificate_type_of_legal_representative ='3' then '警官证'
        |        when t1.certificate_type_of_legal_representative ='4' then '护照'
        |        when t1.certificate_type_of_legal_representative ='5' then '机动车驾驶证'
        |        when t1.certificate_type_of_legal_representative ='6' then '港澳通行证'
        |        when t1.certificate_type_of_legal_representative ='7' then '台胞证'
        |        when t1.certificate_type_of_legal_representative ='9' then '其他国家认可的有效证件'
        |        else '未知证件类型' end certificate_type_of_legal_representative,  -- 运政工商信息-法定代表人证件类型
        |	  t1.phone_number_of_legal_representative, -- 运政工商信息-法人联系电话
        |	  t1.mobile_phone_number_of_legal_representative, -- 运政工商信息-法人手机
        |	  t1.organization_code_card, -- 运政工商信息-组织机构代码证
        |	  t1.tax_id, -- 运政工商信息-税务登记号
        |	  t1.registered_capital, -- 运政工商信息-注册资本
        |	  t1.issuing_date, -- 运政工商信息-发证日期
        |	  t1.organization_code, -- 运政工商信息-组织机构代码
        |   t3.business_operation_range  --运政经营范围（补充企业一级行业）
        |FROM
        |(SELECT
        |      business_owner_id,
        |      business_owner_economic_category,
        |      case WHEN trim(business_registration_number) != '' and business_registration_number is not NULL then trim(business_registration_number)
        |           WHEN (trim(business_registration_number) = '' or business_registration_number is NULL ) and (trim(organization_code_card) != '' and organization_code_card is not NULL) then trim(organization_code_card)
        |           WHEN (trim(business_registration_number) = '' or business_registration_number is NULL)  and (trim(organization_code_card) = '' or organization_code_card is NULL ) and (trim(organization_code) != '' and organization_code is not NULL) then trim(organization_code)
        |           END social_credit_code, --统一社会信用代码
        |      legal_representative, -- 运政工商信息-法定代表人
        |	     legal_representative_number_of_id_certificate, -- 运政工商信息-法人身份证
        |	     business_registration_number, -- 运政工商信息-工商注册号
        |	     certificate_type_of_legal_representative,  -- 运政工商信息-法定代表人证件类型
        |	     phone_number_of_legal_representative, -- 运政工商信息-法人联系电话
        |	     mobile_phone_number_of_legal_representative, -- 运政工商信息-法人手机
        |	     organization_code_card, -- 运政工商信息-组织机构代码证
        |	     tax_id, -- 运政工商信息-税务登记号
        |	     registered_capital, -- 运政工商信息-注册资本
        |	     issuing_date, -- 运政工商信息-发证日期
        |	     organization_code -- 运政工商信息-组织机构代码
        |   from
        |      dwd.dwd_yz_business_info
        |         where trim(business_owner_id) != ''
        |   and business_owner_id is not null
        |   and (LENGTH(TRIM(organization_code)) >1
        |   or LENGTH(TRIM(organization_code_card)) >1
        |   or LENGTH(TRIM(business_registration_number)) >1)
        | )t1
        | right JOIN
        | (SELECT
        |  DISTINCT business_owner_id,
        |  business_owner_name enterprise_name,
        |  address,
        |  if(administrative_division_code ='花溪区' ,'520111',administrative_division_code) area,
        |
        |  administrative_organization_id, -- 运政-行政机构ID
        |	 '' administrative_organization_name, -- 运政-行政机构名称
        |	 whether_branch, -- 运政-是否分支:1是
        |	 business_owner_code, -- 运政-业户代码
        |	 business_owner_status, -- 运政-业户状态
        |	 phone_number, -- 运政-联系电话
        |	 postal_code, -- 运政-邮政编码
        |	 email, -- 运政-电子邮箱
        |	 remark, -- 运政-备注
        |	 department, -- 运政-所属部门
        |	 fax_number, -- 运政-传真电话
        |	 organization_of_created_file, -- 运政-建档机构
        |	 create_date, -- 运政-创建日期
        |	 operator_name, -- 运政-经办人姓名
        |	 operator_phone_number, -- 运政-经办人电话
        |	 name_of_the_person_in_charge -- 运政-负责人姓名
        |from dwd.dwd_yz_company_info
        |WHERE TRIM(business_owner_id) !=''
        |and business_owner_id is not null) t2
        |on t1.business_owner_id = t2.business_owner_id
        |left join
        | (
        | select * from
        |   (SELECT
        |     business_owner_id,
        |     effective_date,
        |     expire_date,
        |     certificate_issuing_authority,
        |     operating_status,
        |     permit_certificate_classification_number,
        |     permit_certificate_classification_abbreviation,
        |     operating_nature,
        |     business_operation_range,
        |     business_owner_type,
        |     issuing_date,
        |     certificate_initial_collection_date,
        |     remark,
        |     maintenance_category,
        |     vehicle_driver_training_institution_level,
        |     ROW_NUMBER() OVER(PARTITION BY business_owner_id ORDER BY expire_date DESC) rk
        |from dwd.dwd_yz_business_certificate_info
        |WHERE TRIM(business_owner_id) !=''
        |and business_owner_id is not null ) x
        |where rk=1
        |
        |) t3
        |on t2.business_owner_id = t3.business_owner_id
        |""".stripMargin).createOrReplaceTempView("yz_enterprise_table")
    session.read.format("jdbc")
      .option("url", ApolloConst.jgdMysqlURL)
      .option("user", ApolloConst.jgdMysqlUserName)
      .option("password", ApolloConst.jgdMysqlPassWord)
      .option("dbtable", "zcov.basic_enterprise_info")
      .option("driver", ApolloConst.jgdMysqlDriver)
      .load().map { row =>
      val social_credit_code: String = row.getAs[String]("social_credit_code")
      val enterprise_code: String = row.getAs[String]("enterprise_code")
      val one_industry_codes: String = row.getAs[String]("one_industry_codes")
      val two_industry_codes: String = row.getAs[String]("two_industry_codes")
      val into_time: String = row.getAs[Timestamp]("gmt_create").toString
      val is_management: Int = row.getAs[Int]("managed")
      val is_sync: Int = row.getAs[Int]("sync")
      (social_credit_code, enterprise_code, one_industry_codes, two_industry_codes, into_time, is_management, is_sync)
    }.toDF("social_credit_code", "enterprise_code", "one_industry_codes", "two_industry_codes", "into_time", "is_management", "is_sync")
      .createOrReplaceTempView("qiyeyun_enterprise_table")

    session.read.format("jdbc")
      .option("url", ApolloConst.jgdMysqlURL)
      .option("user", ApolloConst.jgdMysqlUserName)
      .option("password", ApolloConst.jgdMysqlPassWord)
      .option("dbtable", "zccp.app_category")
      .option("driver", ApolloConst.jgdMysqlDriver)
      .load().map { row =>
      val category_name: String = row.getAs[String]("category_name")
      val one_industry_codes: String = row.getAs[String]("category_code")
      val gmt_modified: String = row.getAs[Timestamp]("gmt_modified").toString
      (category_name, one_industry_codes, gmt_modified)
    }.toDF("category_name", "one_industry_codes", "gmt_modified")
      .createOrReplaceTempView("app_category")

    val frame2 = session.sql(
      """
        |select
        |  t1.enterprise_name, -- 运政-企业名称
        |  t1.social_credit_code, --统一社会信用代码
        |  t1.enterprise_code, --
        |  t1.business_owner_economic_category, --运政-经营业户经济类型
        |  t1.area, --运政-区地区编码
        |  t1.address, --运政-联系地址
        |  t1.administrative_organization_id, -- 运政-行政机构ID
        |	 t1.administrative_organization_name , -- 运政-行政机构名称
        |	 t1.whether_branch, -- 运政-是否分支:1是
        |	 t1.business_owner_code, -- 运政-业户代码
        |	 t1.business_owner_status, -- 运政-业户状态
        |	 t1.phone_number, -- 运政-联系电话
        |	 t1.postal_code, -- 运政-邮政编码
        |	 t1.email, -- 运政-电子邮箱
        |	 t1.remark, -- 运政-备注
        |	 t1.department, -- 运政-所属部门
        |	 t1.fax_number, -- 运政-传真电话
        |	 t1.organization_of_created_file, -- 运政-建档机构
        |	 t1.create_date, -- 运政-创建日期
        |	 t1.operator_name, -- 运政-经办人姓名
        |	 t1.operator_phone_number, -- 运政-经办人电话
        |	 t1.name_of_the_person_in_charge, -- 运政-负责人姓名
        |  t1.legal_representative, -- 运政工商信息-法定代表人
        |	 t1.legal_representative_number_of_id_certificate, -- 运政工商信息-法人身份证
        |	 t1.business_registration_number, -- 运政工商信息-工商注册号
        |	 t1.certificate_type_of_legal_representative,  -- 运政工商信息-法定代表人证件类型
        |	 t1.phone_number_of_legal_representative, -- 运政工商信息-法人联系电话
        |	 t1.mobile_phone_number_of_legal_representative, -- 运政工商信息-法人手机
        |	 t1.organization_code_card, -- 运政工商信息-组织机构代码证
        |	 t1.tax_id, -- 运政工商信息-税务登记号
        |	 t1.registered_capital, -- 运政工商信息-注册资本
        |	 t1.issuing_date, -- 运政工商信息-发证日期
        |	 t1.organization_code, -- 运政工商信息-组织机构代码
        |  t1.business_operation_range , --企业经营范围
        |  t2.enterprise_code base_enterprise_code,  -- 基础企业编码
        |  if(t3.one_industry_codes is not null, t3.one_industry_codes, '-1') one_industry_codes, --所属行业（一级），-1: 未知
        |  if(t2.two_industry_codes is not null, t2.two_industry_codes, '-1') two_industry_codes, -- 所属行业（二级），-1：未知
        |  if(t2.social_credit_code is not null, 1, 0 ) is_registered, -- 是否(在企业斑马云)注册:1是0否
        |  t2.into_time, --接入(注册)时间
        |  t2.is_management, -- 是否管理（0否1是）
        |  t2.is_sync -- 是否同步, 0: 否, 1: 是
        |from
        |  yz_enterprise_table t1
        |left join
        |(
        |select  enterprise_code,new_one_industry_codes as one_industry_codes,two_industry_codes,social_credit_code,into_time,is_management,is_sync from qiyeyun_enterprise_table
        |lateral view explode(split(one_industry_codes,',')) tmp as new_one_industry_codes
        |) t2
        |on lower(t1.social_credit_code) = lower(t2.social_credit_code)
        |full join app_category t3
        |on t2.one_industry_codes=t3.one_industry_codes
        |""".stripMargin).toDF()

    frame2.foreachPartition { iter =>
      DbClient.init("jdgMysql", ApolloConst.jgdMysqlDriver, ApolloConst.jgdMysqlURL, ApolloConst.jgdMysqlUserName, ApolloConst.jgdMysqlPassWord)
      val scopeCodeName: Map[String, String] = scopeCodeNameBroadcast.value
      iter.foreach { row =>
        val enterprise_name: String = row.getAs[String]("enterprise_name")
        val social_credit_code: String = row.getAs[String]("social_credit_code")
        val enterprise_code: String = row.getAs[String]("enterprise_code")
        val business_owner_economic_category: String = row.getAs[String]("business_owner_economic_category")
        val area: String = row.getAs[String]("area")
        val address: String = row.getAs[String]("address")
        val administrative_organization_id: String = row.getAs[String]("administrative_organization_id") //运政-行政机构ID
        val administrative_organization_name: String = row.getAs[String]("administrative_organization_name") //运政-行政机构名称
        val whether_branch: String = row.getAs[String]("whether_branch") //运政-是否分支:1是
        val business_owner_code: String = row.getAs[String]("business_owner_code") //运政-业户代码
        val business_owner_status: String = row.getAs[String]("business_owner_status") //运政-业户状态
        val phone_number: String = row.getAs[String]("phone_number") //运政-联系电话
        val postal_code: String = row.getAs[String]("postal_code") //运政-邮政编码
        val email: String = row.getAs[String]("email") //运政-电子邮箱
        val remark: String = row.getAs[String]("remark") //运政-备注
        val department: String = row.getAs[String]("department") //运政-所属部门
        val fax_number: String = row.getAs[String]("fax_number") //运政-传真电话
        val organization_of_created_file: String = row.getAs[String]("organization_of_created_file") //运政-建档机构
        val create_date: String = row.getAs[String]("create_date") //运政-创建日期
        //        val create_date :String = if (createDate != null) {new DateTime(createDate.getTime).toString("yyyy-MM-dd")} else null
        val operator_name: String = row.getAs[String]("operator_name") //运政-经办人姓名
        val operator_phone_number: String = row.getAs[String]("operator_phone_number") //运政-经办人电话
        val name_of_the_person_in_charge: String = row.getAs[String]("name_of_the_person_in_charge") //运政-负责人姓名
        val legal_representative: String = row.getAs[String]("legal_representative") //运政工商信息-法定代表人
        val legal_representative_number_of_id_certificate: String = row.getAs[String]("legal_representative_number_of_id_certificate") //运政工商信息-法人身份证
        val business_registration_number: String = row.getAs[String]("business_registration_number") //运政工商信息-工商注册号
        val certificate_type_of_legal_representative: String = row.getAs[String]("certificate_type_of_legal_representative") //运政工商信息-法定代表人证件类型
        val phone_number_of_legal_representative: String = row.getAs[String]("phone_number_of_legal_representative") //运政工商信息-法人联系电话
        val mobile_phone_number_of_legal_representative: String = row.getAs[String]("mobile_phone_number_of_legal_representative") //运政工商信息-法人手机
        val organization_code_card: String = row.getAs[String]("organization_code_card") //运政工商信息-组织机构代码证
        val tax_id: String = row.getAs[String]("tax_id") //运政工商信息-税务登记号
        val registered_capital: String = row.getAs[String]("registered_capital") //运政工商信息-注册资本
        val issuingDate: String = row.getAs[String]("issuing_date") //运政工商信息-发证日期
        val issuing_date = if (issuingDate != null && Tools.isDate(issuingDate.trim, "yyyy-MM-dd")) {
          issuingDate
        } else null

        val organization_code: String = row.getAs[String]("organization_code") //运政工商信息-组织机构代码

        val base_enterprise_code: String = row.getAs[String]("base_enterprise_code")
        var one_industry_codes: String = row.getAs[String]("one_industry_codes")
        val two_industry_codes: String = row.getAs[String]("two_industry_codes")
        val is_registered: Int = row.getAs[Int]("is_registered")
        val into_time: String = row.getAs[String]("into_time")
        //val into_time :Date =if (Tools.isDate(create_date_string,"yyyy-MM-dd")) {new SimpleDateFormat("yyyy-MM-dd").parse(create_date_string)} else null
        val is_management: Int = row.getAs[Int]("is_management")
        val is_sync: Int = row.getAs[Int]("is_sync")

        if (one_industry_codes == "-1") {
          var business_operation_range: String = row.getAs[String]("business_operation_range")
          business_operation_range = if (business_operation_range != null) {
            business_operation_range.split(",")(0)
          } else {
            ""
          }
          business_operation_range = scopeCodeName.getOrElse(business_operation_range, "未知经营范围")
          if (business_operation_range.contains("包车客运") || business_operation_range.contains("旅游客运") || business_operation_range.contains("班车客运")) {
            //客运
            one_industry_codes = "1012"
          } else if (business_operation_range.contains("危险货物")) {
            //危货
            one_industry_codes = "1011"
          } else if (business_operation_range.contains("普通货物") || business_operation_range.contains("普通货运") || business_operation_range.contains("货物专用运输") || business_operation_range.contains("大型物件运输")) {
            //普货
            one_industry_codes = "1010"
          } else if (business_operation_range.contains("城市公共汽车")) {
            //城市公交
            one_industry_codes = "1005"
          } else if (business_operation_range.contains("巡游出租")) {
            //出租车
            one_industry_codes = "1015"
          } else if (business_operation_range.contains("网络预约出租")) {
            //网约车
            one_industry_codes = "1016"
          } else if (business_operation_range.contains("A1") || business_operation_range.contains("A2") || business_operation_range.contains("A3")
            || business_operation_range.contains("B1") || business_operation_range.contains("B2") || business_operation_range.contains("B3")
            || business_operation_range.contains("C1") || business_operation_range.contains("C2") || business_operation_range.contains("C3") || business_operation_range.contains("C4") || business_operation_range.contains("C5") || business_operation_range.contains("培训")) {
            //驾校
            one_industry_codes = "1002"
          } else {
            one_industry_codes = "-1"
          }
        }

        val fieldList = List(
          enterprise_name,
          social_credit_code,
          enterprise_code,
          business_owner_economic_category,
          area,
          address,
          administrative_organization_id,
          administrative_organization_name,
          whether_branch,
          business_owner_code,
          business_owner_status,
          phone_number,
          postal_code,
          email,
          remark,
          department,
          fax_number,
          organization_of_created_file,
          create_date,
          operator_name,
          operator_phone_number,
          name_of_the_person_in_charge,
          legal_representative,
          legal_representative_number_of_id_certificate,
          business_registration_number,
          certificate_type_of_legal_representative,
          phone_number_of_legal_representative,
          mobile_phone_number_of_legal_representative,
          organization_code_card,
          tax_id,
          registered_capital,
          issuing_date,
          organization_code,
          base_enterprise_code,
          one_industry_codes,
          two_industry_codes,
          is_registered,
          into_time,
          is_management,
          is_sync)
        DbClient.usingDB("jdgMysql") { db: NamedDB =>
          val sqlStr =
            s"""
               |replace into zcov.yz_enterprise_info_archive(
               |enterprise_name,
               |social_credit_code,
               |enterprise_code,
               |business_owner_economic_category,
               |area,
               |address,
               |administrative_organization_id,
               |administrative_organization_name ,
               |whether_branch,
               |business_owner_code,
               |business_owner_status,
               |phone_number,
               |postal_code,
               |email,
               |remark,
               |department,
               |fax_number,
               |organization_of_created_file,
               |create_date,
               |operator_name,
               |operator_phone_number,
               |name_of_the_person_in_charge,
               |legal_representative,
               |legal_representative_number_of_id_certificate,
               |business_registration_number,
               |certificate_type_of_legal_representative,
               |phone_number_of_legal_representative,
               |mobile_phone_number_of_legal_representative,
               |organization_code_card,
               |tax_id,
               |registered_capital,
               |issuing_date,
               |organization_code,
               |base_enterprise_code,
               |one_industry_codes,
               |two_industry_codes,
               |is_registered,
               |into_time,
               |is_management,
               |is_sync
               |) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)
               |""".stripMargin
          try {
            db autoCommit { implicit session =>
              SQL(sqlStr).bind(fieldList: _*).update().apply()
            }
          } catch {
            case _: Exception => error("报错数据：" + fieldList.mkString(","))
          }

        }
      }
    }
  }
}
