package org.example.dataToES

import com.alibaba.fastjson.{JSON, JSONObject}
import org.example.common.Sparking
import org.example.constant.ApolloConst
import org.example.dataToES.RealDataToEs.error
import org.example.utils.Tools.createUUID32
import org.example.utils.{CommonUtils, ZkManager}
import org.apache.spark.sql.SparkSession
import org.apache.spark.streaming.{Seconds, StreamingContext}
import org.apache.spark.streaming.kafka010.{ConsumerStrategies, HasOffsetRanges, KafkaUtils, LocationStrategies, OffsetRange}
import org.elasticsearch.spark.rdd.EsSpark
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat

import scala.collection.mutable
import scala.collection.mutable.{ArrayBuffer, Map}
import scala.util.Random

/**
 * @Description: 给演示环境的智能报警数据，手动生成报警图片和视频
 * @author ldw
 * @date 2022/1/26
 */
object WarnAttachmentTemporaryReal extends Sparking{

  def main(args: Array[String]): Unit = {

    val sparkSession: SparkSession = CommonUtils.getSparkSession()
    val ssc = new StreamingContext(sparkSession.sparkContext, Seconds(10*60))
    val zkManager = ZkManager(ApolloConst.zkKafka)
    val kafkaParams = getKafkaParams(ApolloConst.bootstrap, "WarnAttachment3.2")
    val offsets = zkManager.getBeginOffset(Seq("alarm_details_topic"), "WarnAttachment3.2")
    val inputStream = KafkaUtils.createDirectStream[String, String](
      ssc,
      LocationStrategies.PreferConsistent,
      ConsumerStrategies.Subscribe[String, String](Seq("alarm_details_topic"), kafkaParams, offsets))
    val offsetRanges = new ArrayBuffer[OffsetRange]()
    inputStream.transform { rdd =>
      offsetRanges.clear()
      offsetRanges.append(rdd.asInstanceOf[HasOffsetRanges].offsetRanges: _*)
      rdd
    }.map(x => x.value()).foreachRDD(rdd => {
      if (!rdd.isEmpty()) {
        val newRdd = rdd.map{ x =>
          var data: JSONObject = null
          try {
            data = JSON.parseObject(x)
          } catch {
            case e: Exception => {
              error(s"该条数据格式错误:${x}")
            }
          }
          data
        }.filter(x => null != x).cache()

        val typeList = List("0x00103","0x00104","0x00102","0x00107","0x00f","0x00e","0x002")
        val WarnAttachmentData = newRdd.filter(x => typeList.contains(x.getString("warntype"))) //过滤指定报警类型
          .map { messageInfo =>
            var esMap = mutable.HashMap[String, String]()
            val vehicleno = messageInfo.getString("vehicleno")
            val vehiclecolor = messageInfo.getString("vehiclecolor")

            val warntime = messageInfo.getString("warntime")
            val formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")
            val times: DateTime = DateTime.parse(warntime, formatter)
            val warnTime: String = (times.toDate.getTime / 1000).toString

            val warntype = messageInfo.getString("warntype")
            val infoid = messageInfo.getString("infoid")
            val id = createUUID32(vehicleno + vehiclecolor + warntime)
            val filePath: String = warntype match {
              case "0x00f" => getFilePath("0x00f")
              case "0x00107" => getFilePath("0x00107")
              case "0x00103" => getFilePath("0x00103")
              case "0x00104" => getFilePath("0x00104")
              case "0x00102" => getFilePath("0x00102")
              case "0x002" => getFilePath("0x002")
              case "0x00e" => getFilePath("0x00e")
            }

            esMap += ("vehicleNo" -> vehicleno)
            esMap += ("vehicleColor" -> vehiclecolor)
            esMap += ("deviceId" -> "-")
            esMap += ("warnTime" -> warnTime)
            esMap += ("warnSeq" -> "-")
            esMap += ("id" -> id)
            esMap += ("fileIndex" -> "1")
            esMap += ("fileCount" -> "2")
            esMap += ("filePath" -> filePath)
            esMap += ("INFO_ID" -> infoid)

          }
//        WarnAttachmentData.foreach(x =>
//          println(x)
//        )
        val source = "warn_attach_index" + "/warn_attach_type"
        EsSpark.saveToEs(WarnAttachmentData.filter(x => !x.isEmpty), source, Map("es.mapping.id" -> "id"))
        zkManager.saveEndOffset(offsetRanges, "WarnAttachment3.2")
      }
    })
    ssc.start()
    ssc.awaitTermination()
  }

  def getFilePath(warnType: String): String = {
    val warnType2Driver: Map[String, List[String]] = Map(
      "0x00f" -> List("A", "B", "C"),
      "0x00107" -> List("A", "B", "C", "D"),
      "0x00103" -> List("A", "B", "C"),
      "0x00104" -> List("A", "B", "C"),
      "0x00102" -> List("A", "B", "C", "D"),
      "0x002" -> List("A", "B", "C", "E"),
      "0x00e" -> List("A", "B")
    )
    val driver2FilePath: Map[String, List[String]] = Map(
      "0x00f-A" -> List("/group1/M00/00/18/CsXs6GHqaL2AKf-fAAMJq8Ihm8E070.jpg,/group1/M00/00/18/CsXs6GHqaQuASSVkAAtlDWYf6JY194.mp4",
        "/group1/M00/00/18/CsXs6GHqaS-Ae5iyAALqaxgIuew482.jpg,/group1/M00/00/18/CsXs6GHqaVOASyF3AAt8I6qcZaw432.mp4",
        "/group1/M00/00/18/CsXs6GHqaWmAUrajAALsFZptm30015.jpg,/group1/M00/00/18/CsXs6GHqaXqAJH6MAArX2OR2axw465.mp4",
        "/group1/M00/00/18/CsXs6GHqaYqAYnxvAAMU9ox95Y0667.jpg,/group1/M00/00/18/CsXs6GHqaaOAdR6CAAtTReqbUSQ234.mp4",
        "/group1/M00/00/18/CsXs6GHqacSAYypYAAMdAtLxLDg996.jpg,/group1/M00/00/18/CsXs6GHqadCAAiirAAtGWlfSZDU190.mp4"),
      "0x00f-B" -> List("/group1/M00/00/18/CsXs6GHqafuAfcJVAAMysGmwkZg493.jpg,/group1/M00/00/18/CsXs6GHqagiAT0BFAAt6iq-cCZQ704.mp4",
        "/group1/M00/00/18/CsXs6GHqaheACBy6AAMcj6ndJPA037.jpg,/group1/M00/00/18/CsXs6GHqaiaAfTNwAAvFxopoFOg771.mp4",
        "/group1/M00/00/18/CsXs6GHqajmAHYJWAAM4avTDgmA301.jpg,/group1/M00/00/18/CsXs6GHqakmAYLqqAAtL5HxXJFg025.mp4",
        "/group1/M00/00/18/CsXs6GHqamKAN0o9AAM4gZxAsXE109.jpg,/group1/M00/00/18/CsXs6GHqaniANXQ7AAs_uygaTPY053.mp4",
        "/group1/M00/00/18/CsXs6GHqapiAU8m6AAMPhI9a8P4079.jpg,/group1/M00/00/18/CsXs6GHqaqaAHgMxAAtWxf41cQ8358.mp4"),
      "0x00f-C" -> List("/group1/M00/00/18/CsXs6GHqar2AM0nyAAE66p5Y39E097.jpg,/group1/M00/00/18/CsXs6GHqasuAQH37AAncJBFOV_g540.mp4",
        "/group1/M00/00/18/CsXs6GHqau6AfdtKAAFm7N-gn1Q421.jpg,/group1/M00/00/18/CsXs6GHqawCAPmQpAAoDkGbIKcY029.mp4",
        "/group1/M00/00/18/CsXs6GHqaw6AFv7mAAE0eJb3I9A676.jpg,/group1/M00/00/18/CsXs6GHqaxyAB27kAAlufMcyFsk024.mp4",
        "/group1/M00/00/18/CsXs6GHqayuAHHa5AAE9YOsjH6A842.jpg,/group1/M00/00/18/CsXs6GHqaziAYiiRAAmOuEBBnnU436.mp4",
        "/group1/M00/00/18/CsXs6GHqa0qAJEUWAAEk3mSd4to829.jpg,/group1/M00/00/18/CsXs6GHqa2GAR-tBAAnKjhoXvP4081.mp4"),
      "0x00107-A" -> List("/group1/M00/00/18/CsXs6GHqa4OAbfuNAAMS2qXrOF4336.jpg,/group1/M00/00/18/CsXs6GHqa5CAP9pBAAvH0jX3lYI509.mp4",
        "/group1/M00/00/18/CsXs6GHqa52AE9-DAAMasqAsMA4142.jpg,/group1/M00/00/18/CsXs6GHqa62AFPHjAAvT01K9jr0596.mp4",
        "/group1/M00/00/18/CsXs6GHqa72Ab9WoAAMuFLIvhho796.jpg,/group1/M00/00/18/CsXs6GHqa8uAUugUAAtwdC_RLF8203.mp4",
        "/group1/M00/00/18/CsXs6GHqa9qAUmnBAAMLBDUgylo435.jpg,/group1/M00/00/18/CsXs6GHqa-qAOl7nAAuYZfJ2q-w330.mp4",
        "/group1/M00/00/18/CsXs6GHqbAaAdAW8AANB8VcwhRU961.jpg,/group1/M00/00/18/CsXs6GHqbBWAVJglAAvaVslp8tg499.mp4"),
      "0x00107-B" -> List("/group1/M00/00/18/CsXs6GHqbC2AZQCHAAL-7VQ_opw915.jpg,/group1/M00/00/18/CsXs6GHqbDuAfZhfAAs9rE_XM3E857.mp4",
        "/group1/M00/00/18/CsXs6GHqbEyAJ0YUAAML1QAwCrA923.jpg,/group1/M00/00/18/CsXs6GHqbFuAcAc5AAuO365f0y4081.mp4",
        "/group1/M00/00/18/CsXs6GHqbGyAcv7kAAMFXhuJy9s191.jpg,/group1/M00/00/18/CsXs6GHqbHmARb0dAAutfkzw3Bw691.mp4",
        "/group1/M00/00/18/CsXs6GHqbJOAbLheAAMhtxTWAHs696.jpg,/group1/M00/00/18/CsXs6GHqbKGAek-EAAvVOERE1f4439.mp4",
        "/group1/M00/00/18/CsXs6GHqbK6AGuhhAAMIyLzADmY236.jpg,/group1/M00/00/18/CsXs6GHqbL2AR7cIAAu2Bo3o-Sg770.mp4",
        "/group1/M00/00/18/CsXs6GHqbMyAZ6RkAAMIadF0bXI948.jpg,/group1/M00/00/18/CsXs6GHqbNmAfxYvAAuooLyTvko230.mp4"),
      "0x00107-C" -> List("/group1/M00/00/18/CsXs6GHqbOyAYtUTAAMWX0RFZMA627.jpg,/group1/M00/00/18/CsXs6GHqbPqAE8lJAAr2ls0UGs4324.mp4",
        "/group1/M00/00/18/CsXs6GHqbQuAfTc0AAMA1xnDPKY147.jpg,/group1/M00/00/18/CsXs6GHqbRuAGboWAAtlFAFtek8139.mp4",
        "/group1/M00/00/18/CsXs6GHqbSmAd_RKAAM6s4lgfhY170.jpg,/group1/M00/00/18/CsXs6GHqbTmAb-_WAAvbY64R-98630.mp4",
        "/group1/M00/00/18/CsXs6GHqbUiASqqFAANiJfJchWw523.jpg,/group1/M00/00/18/CsXs6GHqbVqAGyDvAAvmMO0vYx0935.mp4",
        "/group1/M00/00/18/CsXs6GHqbWyAO0YjAAMm3W_FDDk163.jpg,/group1/M00/00/18/CsXs6GHqbXeAIYIoAAtgpvhlAfI886.mp4"),
      "0x00107-D" -> List("/group1/M00/00/18/CsXs6GHqbZWAJXuxAAQY05QAt30667.jpg,/group1/M00/00/18/CsXs6GHqbaGAEmAZAAtcH_9rHA0758.mp4",
        "/group1/M00/00/18/CsXs6GHqbbWAEzKWAAPKreWUY-4044.jpg,/group1/M00/00/18/CsXs6GHqbcOAe-PGAAttKe1xEOo535.mp4",
        "/group1/M00/00/18/CsXs6GHqbdOAHKQmAAPGd3dSco0720.jpg,/group1/M00/00/18/CsXs6GHqbe-AEatZAAtSkbc8R2I676.mp4",
        "/group1/M00/00/18/CsXs6GHqbgCAZIMXAAPHYF5hMNk012.jpg,/group1/M00/00/18/CsXs6GHqbg-ARZL3AAsuAd0Mb0k921.mp4",
        "/group1/M00/00/18/CsXs6GHqbiWAIFDtAAPMa2bGyHM336.jpg,/group1/M00/00/18/CsXs6GHqbjiAbohtAAtt4lh-Zek193.mp4"),
      "0x00103-A" -> List("/group1/M00/00/18/CsXs6GHqbn-AMP-_AAVsNuYRyeA291.jpg,/group1/M00/00/18/CsXs6GHqbpGAHoSOABGEh5PZ9RM637.mp4",
        "/group1/M00/00/18/CsXs6GHqbr2ARvzfAAVMlFPbtWg611.jpg,/group1/M00/00/18/CsXs6GHqbsqAVYI6ABHYgIQjc4Y182.mp4",
        "/group1/M00/00/18/CsXs6GHqbtmAKbhtAAWQB8OP-kc784.jpg,/group1/M00/00/18/CsXs6GHqbuuAb_ELABGX63I4o5k536.mp4",
        "/group1/M00/00/18/CsXs6GHqbv-ARmYQAAVitCazFvY483.jpg,/group1/M00/00/18/CsXs6GHqbwmAATfYABGC18B4vQg137.mp4",
        "/group1/M00/00/18/CsXs6GHqbx6AAA0mAAZTO3Rejic121.jpg,/group1/M00/00/18/CsXs6GHqbyqAOEYxABFIBGt9QjM987.mp4",
        "/group1/M00/00/18/CsXs6GHqbzmATJmcAAaE3aKKWOE051.jpg,/group1/M00/00/18/CsXs6GHqb1eAQyjhABJNzvk4nOI672.mp4"),
      "0x00103-B" -> List("/group1/M00/00/18/CsXs6GHqb2iALiaGAASUrjJJvv8602.jpg,/group1/M00/00/18/CsXs6GHqb36AdUG5ABGzUN8sVn8647.mp4",
        "/group1/M00/00/18/CsXs6GHqb4yAf0ASAATDXGtDu10685.jpg,/group1/M00/00/18/CsXs6GHqb5mAG-pFABCIc57ahqo320.mp4",
        "/group1/M00/00/19/CsXs6GHqb6WAPj9_AAWPKUYumzo860.jpg,/group1/M00/00/19/CsXs6GHqb7iASgUYABFshgRiAmI939.mp4",
        "/group1/M00/00/19/CsXs6GHqb8uAQVJbAAXfLzB7yCc626.jpg,/group1/M00/00/19/CsXs6GHqb9eAbnSHABJJjYDxT-0173.mp4",
        "/group1/M00/00/19/CsXs6GHqb-WAGETJAATN573F2aw158.jpg,/group1/M00/00/19/CsXs6GHqb_OAV1XvABCvUJL5fiQ484.mp4",
        "/group1/M00/00/19/CsXs6GHqcAWARE6OAAU2a9a-1LY385.jpg,/group1/M00/00/19/CsXs6GHqcB2ABnxwABHoVEi3QIY574.mp4"),
      "0x00103-C" -> List("/group1/M00/00/19/CsXs6GHqcDOAHIHxAAWGoaxe-vg723.jpg,/group1/M00/00/19/CsXs6GHqcEOAdMKoABG49tOvjb8600.mp4",
        "/group1/M00/00/19/CsXs6GHqcFmAXQ_9AAYKkur6Puw368.jpg,/group1/M00/00/19/CsXs6GHqcGiAZhWJABHePB6EC9s046.mp4",
        "/group1/M00/00/19/CsXs6GHqcHqAcJ8pAAYLWqm7LVs734.jpg,/group1/M00/00/19/CsXs6GHqcIuAONUxABIHt8rPVto729.mp4",
        "/group1/M00/00/19/CsXs6GHqcJmAE0PtAAWLad6EPUQ508.jpg,/group1/M00/00/19/CsXs6GHqcKmAVujhABF2X0SV4yY796.mp4",
        "/group1/M00/00/19/CsXs6GHqcLeAT5ohAAVfqoM7BGY382.jpg,/group1/M00/00/19/CsXs6GHqcMWAfPIcABF47XiEBR0714.mp4"),
      "0x00104-A" -> List("/group1/M00/00/19/CsXs6GHqcOCAUGthAATmYJYYIa4912.jpg,/group1/M00/00/19/CsXs6GHqcO-ADR5AAAvTbWsHmt4676.mp4",
        "/group1/M00/00/19/CsXs6GHqcP2AEYGIAAUTDzNIhSQ511.jpg,/group1/M00/00/19/CsXs6GHqcQ6AYIh6AAvSRFRSw1A918.mp4",
        "/group1/M00/00/19/CsXs6GHqcRyAQ-4kAAVfLsMyKGM295.jpg,/group1/M00/00/19/CsXs6GHqcSyAa0dsAAvTJ5zPat8600.mp4",
        "/group1/M00/00/19/CsXs6GHqcT6ARt8KAAWWNwaU80M459.jpg,/group1/M00/00/19/CsXs6GHqcVOAGOJwAAuVWJ8DUgI354.mp4",
        "/group1/M00/00/19/CsXs6GHqcWGAPF_AAAVOeF4M2A0563.jpg,/group1/M00/00/19/CsXs6GHqcXiAfh6zAAvHmhvjBzY714.mp4"),
      "0x00104-B" -> List("/group1/M00/00/19/CsXs6GHqcY6AQ0cMAAU_1AlkPvg147.jpg,/group1/M00/00/19/CsXs6GHqcZ2AQVZZAAt0kQ14bxA371.mp4",
        "/group1/M00/00/19/CsXs6GHqcauAeXjVAAUthKgIWwc527.jpg,/group1/M00/00/19/CsXs6GHqcbuAWeshAAuBfbIB5Qs222.mp4",
        "/group1/M00/00/19/CsXs6GHqccyAOCDgAAVDMfW1zMA141.jpg,/group1/M00/00/19/CsXs6GHqcdyAYk1aAAuSc4n1Rvs183.mp4",
        "/group1/M00/00/19/CsXs6GHqcfiAI02XAAUfnQNrx9E534.jpg,/group1/M00/00/19/CsXs6GHqcgSAAvKlAAv9pk0FaNg915.mp4",
        "/group1/M00/00/19/CsXs6GHqchOAW9glAAVVT2WcSjg244.jpg,/group1/M00/00/19/CsXs6GHqciiAWIKWAAuz6-H1yeM515.mp4"),
      "0x00104-C" -> List("/group1/M00/00/19/CsXs6GHqckCAWBQ4AAdfwTEmGQU690.jpg,/group1/M00/00/19/CsXs6GHqclOAApfRAAhviceRcW0643.mp4",
        "/group1/M00/00/19/CsXs6GHqcmaATJkhAAbC4E3O1cI400.jpg,/group1/M00/00/19/CsXs6GHqcnSANjTbAAqyYX-NRUs196.mp4",
        "/group1/M00/00/19/CsXs6GHqcouAQYPqAAd8TpFYlVI799.jpg,/group1/M00/00/19/CsXs6GHqcpaATJ0rAAvbe2Xy97U515.mp4",
        "/group1/M00/00/19/CsXs6GHqcrOAID19AAae2ThpsMc588.jpg,/group1/M00/00/19/CsXs6GHqcsOAXj0AAAu-IRAqEtE830.mp4",
        "/group1/M00/00/19/CsXs6GHqctWAOQZFAAZ3dUEdyBc137.jpg,/group1/M00/00/19/CsXs6GHqcvGADqfgAAv7RuNWuEI571.mp4"),
      "0x00102-A" -> List("/group1/M00/00/19/CsXs6GHqcwiAYK91AAWt0EWHgZk169.jpg,/group1/M00/00/19/CsXs6GHqcxqAPZpBAAwLit5OH6s279.mp4",
        "/group1/M00/00/19/CsXs6GHqczCASvLFAATxywMo6yM051.jpg,/group1/M00/00/19/CsXs6GHqcz6AcoWYAAvFzubMc-4857.mp4",
        "/group1/M00/00/19/CsXs6GHqc1SAQdFbAAUCVzie278805.jpg,/group1/M00/00/19/CsXs6GHqc2KAR4QBAAvqfiMw0yM772.mp4",
        "/group1/M00/00/19/CsXs6GHqc3GAMGN1AAVBYsEM3Qk671.jpg,/group1/M00/00/19/CsXs6GHqc36AAwYdAAvEypqgMxY483.mp4",
        "/group1/M00/00/19/CsXs6GHqc4yAKRTTAAVaaT61j6E983.jpg,/group1/M00/00/19/CsXs6GHqc5qAPV6-AAvAcsmGWno447.mp4"),
      "0x00102-B" -> List("/group1/M00/00/19/CsXs6GHqc6uAM4PDAAV85X0SSWw700.jpg,/group1/M00/00/19/CsXs6GHqc7qAHYHDAAsqPCQhKd0442.mp4",
        "/group1/M00/00/19/CsXs6GHqc8yAF1pfAASQtnHymXA449.jpg,/group1/M00/00/19/CsXs6GHqc9iAEG9nAAu_rxgj8CU698.mp4",
        "/group1/M00/00/19/CsXs6GHqc-yAdUuaAAUsV_vjyxE717.jpg,/group1/M00/00/19/CsXs6GHqc_iASAunAAuai9YuyHM882.mp4"),
      "0x00102-C" -> List("/group1/M00/00/19/CsXs6GHqdE6ATGllAASNDUb3Q1I423.jpg,/group1/M00/00/19/CsXs6GHqdGGAWK7dAAtWH-UTBAk838.mp4",
        "/group1/M00/00/19/CsXs6GHqdHGALLPXAAW-I0J3lro787.jpg,/group1/M00/00/19/CsXs6GHqdH6AaVeSAAsqFr4e9fY955.mp4",
        "/group1/M00/00/19/CsXs6GHqdJCAVw_2AAVtvUnrfvo885.jpg,/group1/M00/00/19/CsXs6GHqdJ2Afqv5AAsrbQAt14I017.mp4"),
      "0x00102-D" -> List("/group1/M00/00/19/CsXs6GHqdLOAFCAuAAWKIDWN15c870.jpg,/group1/M00/00/19/CsXs6GHqdMKAVvFQAAvH1m4kKMw421.mp4",
        "/group1/M00/00/19/CsXs6GHqdNaAed1pAAWaVuhW5NY674.jpg,/group1/M00/00/19/CsXs6GHqdOOAAMqBAAvnr4r9bCY833.mp4",
        "/group1/M00/00/19/CsXs6GHqdPKAaCnSAAWObZQMJGo953.jpg,/group1/M00/00/19/CsXs6GHqdQaAaErxAAuM4EQb_Tg389.mp4",
        "/group1/M00/00/19/CsXs6GHqdReAJoyfAAZUEV-ufXU796.jpg,/group1/M00/00/19/CsXs6GHqdSeAbUZJAAtp2Z4d6Y4772.mp4",
        "/group1/M00/00/19/CsXs6GHqdTWAK8Q7AAWHiQyxjFQ689.jpg,/group1/M00/00/19/CsXs6GHqdUyAXZMLAAs4wAR5lG4129.mp4"),
      "0x002-A" -> List("/group1/M00/00/19/CsXs6GHqdWWAGtynAANHIuDiYoY637.jpg,/group1/M00/00/19/CsXs6GHqdXSAW3_SAAt2S0fK8Zo951.mp4",
        "/group1/M00/00/19/CsXs6GHqdYaAT5bpAAMwpxftenU011.jpg,/group1/M00/00/19/CsXs6GHqdZeACmhgAAuMLr_4IeU989.mp4",
        "/group1/M00/00/19/CsXs6GHqdayAMIcjAAM_R4EMBz0494.jpg,/group1/M00/00/19/CsXs6GHqdcCAGH5sAAtjsa2YDFY308.mp4",
        "/group1/M00/00/19/CsXs6GHqddSAO21cAANMsLwiNFM352.jpg,/group1/M00/00/19/CsXs6GHqdd-AdFEEAAtvqHxUsrE827.mp4",
        "/group1/M00/00/19/CsXs6GHqde6ACZs4AAMQ22Y3m3k058.jpg,/group1/M00/00/19/CsXs6GHqdgCAJ0mTAAu_qgDhayc287.mp4"),
      "0x002-B" -> List("/group1/M00/00/19/CsXs6GHqdhGAXW3GAASJMOu6RuM161.jpg,/group1/M00/00/19/CsXs6GHqdh6ABXd9AAuI1KJU7iE612.mp4"),
      "0x002-C" -> List("/group1/M00/00/19/CsXs6GHqdi-AZ7e_AAOoETqgn9I557.jpg,/group1/M00/00/19/CsXs6GHqdj6AVjkcAAtE8zBDCo0267.mp4",
        "/group1/M00/00/19/CsXs6GHqdk6Aei0xAAO6FHzanlY881.jpg,/group1/M00/00/19/CsXs6GHqdl2AKPgdAAtNPMhkuHs669.mp4"),
      "0x002-D" -> List("/group1/M00/00/19/CsXs6GHqdrmAF5rAAANVQAXe7LY538.jpg,/group1/M00/00/19/CsXs6GHqdsiATYEwABENDiwxMFc380.mp4",
        "/group1/M00/00/19/CsXs6GHqdt2ASIwaAANKQ0vaZSQ332.jpg,/group1/M00/00/19/CsXs6GHqduiAPWuFABDj8c_t3MQ346.mp4",
        "/group1/M00/00/1A/CsXs6GHqdvmAYscBAAMrziBlFu4878.jpg,/group1/M00/00/1A/CsXs6GHqdwSAQMOJABEY6C2yxXY418.mp4",
        "/group1/M00/00/1A/CsXs6GHqdxyAQ99yAAOgjw1AkdY829.jpg,/group1/M00/00/1A/CsXs6GHqdyiAFrj3ABEqxwR5kLE464.mp4",
        "/group1/M00/00/1A/CsXs6GHqdzmAXF_3AAMnGXTOIb4503.jpg,/group1/M00/00/1A/CsXs6GHqd0eAe-4oABIOYp7HfUU431.mp4"),
      "0x002-E" -> List("/group1/M00/00/1A/CsXs6GHqd1qAXNRLAAQfJF9D0T8768.jpg,/group1/M00/00/1A/CsXs6GHqd2eAAUd9AAtbc__X_6o779.mp4",
        "/group1/M00/00/1A/CsXs6GHqd3WAT8t_AAQhdgISYpo618.jpg,/group1/M00/00/1A/CsXs6GHqd4SASOq0AAtTbD334dA046.mp4",
        "/group1/M00/00/1A/CsXs6GHqd6GAU7mEAAQjgleiTBk815.jpg,/group1/M00/00/1A/CsXs6GHqd62AdT-nAAt4h_-wMWg623.mp4",
        "/group1/M00/00/1A/CsXs6GHqd7-AaI2sAAQcE8ltLCI251.jpg,/group1/M00/00/1A/CsXs6GHqd8-AfXchAAtu6vqExl0589.mp4",
        "/group1/M00/00/1A/CsXs6GHqd9-ADK9jAAQXFZdFW5Q093.jpg,/group1/M00/00/1A/CsXs6GHqd-2ASFrpAAs8asWj4es812.mp4"),
      "0x00e-A" -> List("/group1/M00/00/1A/CsXs6GHqeAGAFHkyAAQQg0uVCKI189.jpg,/group1/M00/00/1A/CsXs6GHqeBKAdprUAAq_Q0_G54I953.mp4"),
      "0x00e-B" -> List("/group1/M00/00/1A/CsXs6GHqeCiAe9STAASgMq6RLYY834.jpg,/group1/M00/00/1A/CsXs6GHqeECAPdZYAAraFmIZOXM774.mp4"),
      "0x00e-C" -> List("/group1/M00/00/1A/CsXs6GHqeFGAO5txAASsRCxD1Nk029.jpg,/group1/M00/00/1A/CsXs6GHqeGWAcuaEAAr0X-Njk08989.mp4")
    )
    val random = new Random()
    val driverList: List[String] = warnType2Driver(warnType)
    val num = random.nextInt(driverList.size)
    val driverNum = driverList(num)

    val filePathList: List[String] = driver2FilePath(warnType + "-" + driverNum)
    val num1 = random.nextInt(filePathList.size)
    val filePath = filePathList(num1)
    filePath
  }
}
