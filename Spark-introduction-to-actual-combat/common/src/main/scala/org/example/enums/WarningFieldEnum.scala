package org.example.enums

object WarningFieldEnum extends Enumeration {
  type WarningFieldEnum = Value

  //报警次数
  val WARN_TIMES = Value("warnTimes")
  //是否下坡
  val IS_DOWN_HILL = Value("isDownhill")
  //连续报警次数
  val CONTINUOUS_WARN_TIMES = Value("continuousWarnTimes")
  //连续驾驶时长
  val CONTINUOUS_DRIVING_TIME = Value("continuousDrivingTime")
  //是否追加报警记录
  val IS_ADD_ALARM = Value("isAddAlarm")
}
