package org.example.enums

object WarningTypeEnum extends Enumeration {
  type WarningTypeEnum = Value

  //疲劳驾驶报警
  val FATIGUE_DRIVING = Value("fatigueDriving")
  //使用手机报警
  val USE_PHONE = Value("answerHandheldPhoneCalls")
  //抽烟报警
  val SMOKING = Value("smoke")
  //分神报警
  val DISTRACTION = Value("distractedDriving")
  //前向碰撞报警
  val FORWARD_COLLISION = Value("forwardCollision")
  //车距过近报警
  val CAR_TOO_CLOSE = Value("tooClose")
  //车道偏离报警
  val LANE_DEPARTURE = Value("laneDeparture")
  //驾驶员异常报警
  val DRIVER_ABNORMALITY = Value("driverAbnormality")
  //驾驶员身份异常报警
  val ALARM_FOR_ABNORMAL_DRIVER_STATUS = Value("alarmForAbnormalDriverStatus")
  //百码超速报警
  val ONE_HUNDRED_YARD_SPEED = Value("oneHundredYardSpeed")
  //连续百码超速报警
  val CONTINUOUS_HUNDRED_YARDS_SPEEDING = Value("continuousHundredYardsSpeeding")
  //八十码超速报警
  val EIGHTY_YARD_SPEED = Value("eightyYardSpeed")
  //连续八十码超速报警
  val CONTINUOUS_EIGHTY_YARDS_SPEEDING = Value("continuousEightyYardsSpeeding")
  //一百二十码超速报警
  val ONE_HUNDRED_AND_TWENTY_YARDSPEED = Value("oneHundredAndTwentyYardSpeed")
  //连续一百二十码超速报警
  val CONTINUOUS_ONE_HUNDRED_AND_TWENTY_YARDS_SPEEDING = Value("continuousOneHundredAndTwentyYardsSpeeding")
  //夜间八十码超速报警
  val YARD_SPEEDING_ALARM_AT_NIGHT = Value("80-yardSpeedingAlarmAtNight")
  //连续夜间八十码超速报警
  val CONTINUOUS_EIGHTY_YARDS_SPEEDING_AT_NIGHT = Value("continuousEightyYardsSpeedingAtNight")
  //夜间六十码超速报警
  val SIXTY_YARD_SPEEDING_ALARM_AT_NIGHT = Value("60-yardSpeedingAlarmAtNight")
  //连续夜间六十码超速报警
  val CONTINUOUS_SIXTY_YARDS_SPEEDING_AT_NIGHT = Value("continuousSixtyYardsSpeedingAtNight")
  //连续4小时超时报警
  val OVERTIME_ALARM_FOR_4HOURS_OF_CONTINUOUS_DRIVING = Value("overtimeAlarmFor4HoursOfContinuousDriving")
  //夜间连续2小时超时报警
  val OVERTIME_ALARM_FOR_2HOURS_OF_CONTINUOUS_DRIVING_AT_NIGHT = Value("overtimeAlarmFor2HoursOfContinuousDrivingAtNight")
  //2-5点凌晨违规禁行报警
  val CLOCK_IN_THE_MORNING_BAN = Value("2-5clockInTheMorningBan")
  //ADAS设备有效性报警
  val ADAS_DEVICE_VALIDITY_ALARM = Value("ADASDeviceValidityAlarm")
  //DSM设备有效性报警
  val DSM_DEVICE_VALIDITY_ALARM = Value("DSMDeviceValidityAlarm")
  //连续驾驶3.5小时报警
  val OVERTIME_ALARM_FOR_3H_OF_DRIVE = Value("overtimeAlarmFor3.5HOfDrive")
  //gps设备异常报警
  val DEVICE_GOESON_LINE_ABNORMALLY = Value("deviceGoesOnlineAbnormally")
  //新增：GPS设备三天未上线异常报警
  val DEVICE_FOR_3DAYS_OF_GOESON_LINE_ABNORMALLY = Value("deviceFor3DaysOfGoesOnlineAbnormally")
  //智能设备异常报警
  val SMART_EQUIPMENT_ABNORMAL_ALARM = Value("smartEquipmentAbnormalAlarm")
  //新增：智能设备三天未上线异常报警
  val SMART_FOR_3DAYS_OF_EQUIPMENT_ABNORMAL_ALARM = Value("smartFor3DaysOfEquipmentAbnormalAlarm")
  //驾驶人人脸识别设备异常报警
  val DRIVER_FACIAL_RECOGNITION_DEVICE_ABNORMAL = Value("DriverFacialRecognitionDeviceAbnormal")
  //设备异常报警-废弃
  val EQUIPMENT_ABNORMAL_ALARM = Value("equipmentAbnormalAlarm")
}
