package org.example.enums

object ExpressionEnum extends Enumeration {
  //等于equals、小于lessThan、小于等于noMoreThan、大于moreThan、大于等于noLessThan、介于between
  type ExpressionEnum = Value

  //等于equals
  val EXPRESSION_EQUALS = Value("equals")
  //小于lessThan
  val EXPRESSION_LESSTHAN = Value("lessThan")
  //小于等于noMoreThan
  val EXPRESSION_NOMORETHAN = Value("noMoreThan")
  //大于moreThan
  val EXPRESSION_MORETHAN = Value("moreThan")
  //大于等于noLessThan
  val EXPRESSION_NOLESSTHAN = Value("noLessThan")
  //介于between
  val EXPRESSION_BETWEEN = Value("between")
}
