package org.example.dao

import scala.collection.mutable

/**
  * 需要生成风险的信息
  * @param id
  * @param riskName
  * @param riskGradeName
  * @param level
  * @param riskDescription
  * @param riskTypeCode
  * @param vehicleNo
  * @param vehicleColor
  * @param speed
  * @param longitude
  * @param latitude
  * @param warnTime
  * @param riskCodeTime
  * @param warns
  * @param isAddAlarm
  */
case class RiskGenerate(var id: String,
                        var riskName: String,
                        var riskGradeName: String,
                        var level: String,
                        var riskDescription: String,
                        var riskTypeCode: String,
                        var vehicleNo: String,
                        var vehicleColor: String,
                        var speed: String,
                        var longitude: String,
                        var latitude: String,
                        var warnTime: String,
                        var riskCodeTime: String,
                        var warns: mutable.HashMap[String, Iterable[AlarmEvidence]],
                        var isAddAlarm: String,
                        var tripStart: String)