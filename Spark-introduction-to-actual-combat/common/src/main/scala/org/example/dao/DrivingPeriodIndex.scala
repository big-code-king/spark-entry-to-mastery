package org.example.dao

/**
  * 车辆行驶周期
  * @param id
  * @param vehicleNo
  * @param vehicleColor
  * @param licenseNumber
  * @param driverName
  * @param cardLicenseNumber
  * @param cardDriverName
  * @param drivingDuration
  * @param drivingMileage
  * @param tripStartTime
  * @param tripEndTime
  */
case class DrivingPeriodIndex(var id: String,
                              var vehicleNo: String,//车牌号
                              var vehicleColor: String,//车牌颜色
                              var licenseNumber: String,//驾驶人从业证号
                              var driverName: String,//驾驶人姓名
                              var cardLicenseNumber: String,//登签驾驶人从业证号
                              var cardDriverName: String,//登签驾驶人姓名
                              var drivingDuration: Int,//驾驶时长/单位秒
                              var drivingMileage: Double,//驾驶里程/单位公里
                              var tripStartTime: String,//行程开始时间
                              var tripEndTime: String//行程结束时间
                             )