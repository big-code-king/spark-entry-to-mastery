package org.example.dao

case class CustomAlarmEvidence(primaryKey: String,
                               vehicleNo: String,
                               vehicleColor: String,
                               longitude: String,
                               latitude: String,
                               speed: String,
                               driverName: String,
                               driverCode: String,
                               vehicleEnterpriseCode: String,
                               datetime: String,
                               riskCodeTime: String,
                               appId: String,
                               socialCreditCode: String,
                               enterpriseName: String,
                               dataType: String,
                               alarmTime: String,
                               businessTime: String,
                               useNature: String,
                               platformName: String,
                               tripStartMillis: Long //行驶周期开始时间
                              ) extends Serializable

