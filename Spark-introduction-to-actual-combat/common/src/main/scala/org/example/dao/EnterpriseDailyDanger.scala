package org.example.dao

import scala.beans.BeanProperty

case class EnterpriseDailyDanger(@BeanProperty var id:String,
                                 @BeanProperty var vehicleSum:String,
                                 @BeanProperty var riskLevel:String,
                                 @BeanProperty var vehicleEnterprise:String,
                                 @BeanProperty var vehicleEnterpriseCode:String,
                                 @BeanProperty var riskDate:String,
                                 @BeanProperty var updateTime:String,
                                 @BeanProperty var enterpriseRealDagerIndex:String
                                 )
