package org.example.dao

import scala.beans.BeanProperty

case class WarnInfoIndex(@BeanProperty var id:String,
                          @BeanProperty var primaryKey:String,
                          @BeanProperty var districtCode:String,
                          @BeanProperty var infoId:String,
                          @BeanProperty var warnType:String,
                          @BeanProperty var eventType:String,
                          @BeanProperty var typeName:String,
                          @BeanProperty var typeCode:String,
                          @BeanProperty var deviceId:String,
                          @BeanProperty var speed:String,
                          @BeanProperty var datetime:String,
                          @BeanProperty var riskCodeTime:String,
                          @BeanProperty var levelId:String,
                          @BeanProperty var warnSrc:String,
                          @BeanProperty var warnSeq:String,
                          @BeanProperty var longitude:String,
                          @BeanProperty var latitude:String,
                          @BeanProperty var lastLoginTime:String,
                          @BeanProperty var driverName:String,
                          @BeanProperty var driverCode:String,
                          @BeanProperty var vehicleEnterpriseCode:String,
                          @BeanProperty var vehicleNo:String,
                          @BeanProperty var vehicleColor:String,
                          @BeanProperty var misinformation:String,
                          @BeanProperty var drivingDuration:String,//行驶时长
                          @BeanProperty var drivingMileage:String//行驶里程
                        )