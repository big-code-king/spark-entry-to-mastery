package org.example.dao

/**
  * 行驶表
  * @param vehicle_no
  * @param vehicle_color
  * @param driver_code
  * @param driver_name
  * @param start_time
  * @param end_time
  * @param `type`
  */
case class DrivingContinuedIndex(var vehicle_no: String,
                                 var vehicle_color: String,
                                 var driver_code: String,
                                 var driver_name: String,
                                 var start_time: String,
                                 var end_time: String,
                                 var `type`: String
                                )