package org.example.dao

import scala.beans.BeanProperty

/**
  * 车辆行驶周期
  * @param lt
  * @param ln
  * @param dn
  * @param cln
  * @param cdn
  * @param ts
  * @param llt
  * @param lat
  * @param lng
  * @param cm
  */
case class DrivingPeriod(@BeanProperty var lt: Int,//上次累计时长
                         @BeanProperty var lln: String,//上次驾驶人从业证号
                         @BeanProperty var ldn: String,//上次驾驶人姓名
                         @BeanProperty var ln: String,//人脸识别驾驶人从业证号
                         @BeanProperty var dn: String,//人脸识别驾驶人姓名
                         @BeanProperty var cln: String,//ic卡驾驶人从业证号
                         @BeanProperty var cdn: String,//ic卡驾驶人姓名
                         @BeanProperty var ts: String,//行程开始时间
                         @BeanProperty var llt: String,//上次定位时间
                         @BeanProperty var lat: Double,//上次定位经度
                         @BeanProperty var lng: Double,//上次定位纬度
                         @BeanProperty var cm: Double,
                         @BeanProperty var sd: Double//上次定位速度
                         ) extends Serializable