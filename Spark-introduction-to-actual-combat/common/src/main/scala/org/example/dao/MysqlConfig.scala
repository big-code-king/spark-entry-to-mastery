package org.example.dao

/**
  *
  * @param format 格式 jdbc
  * @param url  数据库url
  * @param dbTable  数据库表
  * @param user 数据库用户
  * @param password 数据库密码
  * @param mode 模式。append或者overwrite
  */
case class MysqlConfig(format: String, url: String, dbTable: String, user: String, password: String, mode: String)


/**
 *
 * @param format   格式 jdbc
 * @param url      数据库url
 * @param dbTable  数据库表
 * @param user     数据库用户
 * @param password 数据库密码
 * @param mode     模式。append或者overwrite
 */
case class OracleConfig(format: String, url: String, dbTable: String, user: String, password: String, mode: String)