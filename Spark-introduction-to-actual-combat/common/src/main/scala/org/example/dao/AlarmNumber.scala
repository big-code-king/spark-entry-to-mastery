package org.example.dao

import java.util
/**
 *
 * @param currentWarn
 * @param vehicleNo
 * @param vehicleColor
 * @param speed
 * @param longitude
 * @param latitude
 * @param warnTime
 * @param useNature
 * @param controlType
 * @param tripStart
 * @param warnTypeCodes
 */
case class AlarmNumber(var currentWarn: util.Map[String, String],
                       var vehicleNo: String,
                       var vehicleColor: String,
                       var speed: String,
                       var longitude: String,
                       var latitude: String,
                       var warnTime: String,
                       var useNature: String,
                       var controlType: String,
                       var tripStart: String,
                       var warnTypeCodes: List[String])
