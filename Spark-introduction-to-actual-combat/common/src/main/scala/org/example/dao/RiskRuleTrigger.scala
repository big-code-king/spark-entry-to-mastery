package org.example.dao

/**
 * 风险触发规则
 * @param id
 * @param riskId
 * @param level
 * @param riskRules
 */
case class RiskRuleTrigger(var id: Long,
                           var riskId: Long,
                           var level: String,
                           var riskRules: Option[List[RiskRuleWarning]]
                          )