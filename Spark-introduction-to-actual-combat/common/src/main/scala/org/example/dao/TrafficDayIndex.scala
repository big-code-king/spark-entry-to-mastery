package org.example.dao

import scala.beans.BeanProperty

/**
  * 安全行驶里程天维度统计结果
  */
case class TrafficDayIndex(
                         @BeanProperty var id:String,//es的id
                         @BeanProperty var appId:String,//智能视频企业监控平台的唯一标识号
                         @BeanProperty var enterpriseName:String,//运输企业名称
                         @BeanProperty var enterpriseCode:String,//企业编码
                         @BeanProperty var vehicleNo:String,//车牌号
                         @BeanProperty var vehicleColor:String,//车牌颜色
                         @BeanProperty var dayTime:String,//日期
                         @BeanProperty var driverName:String,//驾驶人
                         @BeanProperty var driverId:String,//	驾驶人身份证号码
                         @BeanProperty var driverTime:String,//驾驶时间
                         @BeanProperty var driveRange:Double,//驾驶里程
                         @BeanProperty var useNature:String,//车辆使用性质
                         @BeanProperty var province:String,//省
                         @BeanProperty var city:String,//城市
                         @BeanProperty var area:String//地区
                       )
