package org.example.dao

import scala.beans.BeanProperty

/**
  * 车辆定位实时信息
  */
case class RealLocation(
                         @BeanProperty var appId:String,//智能视频企业监控平台的唯一标识号
                         @BeanProperty var msgId:String,//业务类型标识
                         @BeanProperty var dataType:String,//子业务类型标识
                         @BeanProperty var vehicleNo:String,//车牌号
                         @BeanProperty var vehicleColor:String,//车牌颜色
                         @BeanProperty var encrypt:String,//加密标识
                         @BeanProperty var businessTime:String,//业务发生时间,格式yyyy-MM-dd HH:mm:ss
                         @BeanProperty var lon:String,//经度，单位为 1*10 - 6度
                         @BeanProperty var lat:String,//纬度，单位为 1*10 - 6度
                         @BeanProperty var vec1:String,//速度，指卫星定位车载终端设备上传的行车速度信息，为必填项， 单位为千米每小时(km/ h)
                         @BeanProperty var vec2:String,//行驶记录速度，指车辆行驶记录设备上传的行车速度信息，单位 为千米每小时(km/ h)
                         @BeanProperty var vec3:String,//车辆当前总里程数，指车辆上传的行车里程数，单位为千米(km)
                         @BeanProperty var direction:String,//方向，0 ～ 359，单位为度(°)，正北为 0，顺时针
                         @BeanProperty var altitude:String,//海拔高度，单位为米(m)
                         @BeanProperty var state:String,//车辆状态
                         @BeanProperty var alarm:String,//报警状态
                         @BeanProperty var dateTime:String,//定位时间,格式yyyy-MM-dd HH:mm:ss
        //                 @BeanProperty var districtcode:String,//	定位区县代码
                         @BeanProperty var drivertime:Long,//安全行驶时长
                         @BeanProperty var driverange:Double,//	安全行驶里程
                         @BeanProperty var mileg:Double,//该时间段内最大里程
                         @BeanProperty var dayTime:String//日期
                       )
