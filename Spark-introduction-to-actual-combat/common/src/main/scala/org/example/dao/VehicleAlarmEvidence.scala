package org.example.dao

import scala.beans.BeanProperty
/**
 * 车报警证据
 * @param field
 * @param alarmEvidence
 */
case class VehicleAlarmEvidence(@BeanProperty var field: String,
                                @BeanProperty var alarmEvidence: java.util.List[AlarmEvidence]) extends Serializable
