package org.example.dao

import scala.beans.BeanProperty

/**
  * 处理意见
  * @param id
  * @param content
  */
case class ProcessComment(@BeanProperty var id: String,
                          @BeanProperty var content: String
                         ) extends Serializable