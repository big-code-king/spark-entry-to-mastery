package org.example.dao

import scala.beans.BeanProperty

case class AlarmEvidence(@BeanProperty var id: String,
                         @BeanProperty var time: String,
                         @BeanProperty var drivingTime: Int) extends Serializable
