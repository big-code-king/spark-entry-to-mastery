package org.example.dao

import scala.beans.BeanProperty


case class DrivingContinued(@BeanProperty var lt: Int,//上次人脸识别累计时长
                            @BeanProperty var ln: String,//人脸识别驾驶人从业证号
                            @BeanProperty var dn: String,//人脸识别驾驶人姓名
                            @BeanProperty var lct: Int,//上次ic卡累计时长
                            @BeanProperty var cln: String,//ic卡驾驶人从业证号
                            @BeanProperty var cdn: String,//ic卡驾驶人姓名
                            @BeanProperty var ts: String,//人脸识别行程开始时间
                            @BeanProperty var cts: String,//ic卡行程开始时间
                            @BeanProperty var llt: String,//上次定位时间
                            @BeanProperty var sd: Double//上次定位速度
                           ) extends Serializable