package org.example.dao

import scala.beans.BeanProperty

case class RiskDetailIndex(@BeanProperty var id: String,
                           @BeanProperty var riskCode: String,
                           @BeanProperty var lastWarnDate: String,
                           @BeanProperty var processStatus: String,
                           @BeanProperty var riskTypeCode: String,
                           @BeanProperty var riskName: String,
                           @BeanProperty var riskGradeCode: String,
                           @BeanProperty var riskGradeName: String,
                           @BeanProperty var riskDate: String,
                           @BeanProperty var riskDescription: String,
                           @BeanProperty var lastWarnAddress: String,
                           @BeanProperty var province: String,
                           @BeanProperty var city: String,
                           @BeanProperty var area: String,
                           @BeanProperty var longitude: String,
                           @BeanProperty var latitude: String,
                           @BeanProperty var vehicleNo: String,
                           @BeanProperty var vehicleColor: String,
                           @BeanProperty var vehicleEnterprise: String,
                           @BeanProperty var vehicleEnterpriseCode: String,
                           @BeanProperty var driverName: String,
                           @BeanProperty var driverCode: String,
                           @BeanProperty var driverEnterprise: String,
                           @BeanProperty var driverEnterpriseCode: String,
                           @BeanProperty var useNature: String,
                           @BeanProperty var contactInfo: String,
                           @BeanProperty var continuousTime: String,
                           @BeanProperty var speed: String,
                           @BeanProperty var warnList: java.util.List[WarnInfoIndex],
                           @BeanProperty var suppressEndtime: String,
                           @BeanProperty var processTypeCode: String,
                           @BeanProperty var processName: String,
                           @BeanProperty var processDescription: String,
                           @BeanProperty var processDate: String,
                           @BeanProperty var processUserCode: String,
                           @BeanProperty var processUserName: String,
                           @BeanProperty var realFlag: String,
                           @BeanProperty var holdFlag: String,
                           @BeanProperty var holdHour: String,
                           @BeanProperty var holdMinutes: String,
                           @BeanProperty var rstartLongitude: String,
                           @BeanProperty var rstartLatitude: String,
                           @BeanProperty var rendLongitude: String,
                           @BeanProperty var rendLatitude: String,
                           @BeanProperty var receiveDate: String,
                           @BeanProperty var processingTime: String,
                           @BeanProperty var cardDriverName: String, //登签驾驶员姓名
                           @BeanProperty var cardDriverLicence: String, //登签驾驶员从业资格证号
                           @BeanProperty var cardDriverEnterpriseCode: String, //登签驾驶员所属企业编号
                           @BeanProperty var cardDriverEnterprise: String, //登签驾驶员所属企业名称
                           @BeanProperty var cardContactInfo: String, //登签驾驶员联系方式
                           @BeanProperty var roadId: String, //道路id
                           @BeanProperty var roadName: String, //道路名称
                           @BeanProperty var generateTime: String, //风险生成时间
                           @BeanProperty var processDescriptionId: String, //处理意见id
                           @BeanProperty var processCommentList: java.util.List[ProcessComment]
                          )