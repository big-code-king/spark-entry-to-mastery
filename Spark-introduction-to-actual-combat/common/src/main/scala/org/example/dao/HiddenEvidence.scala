package org.example.dao

import scala.beans.BeanProperty

/**
 * 隐患证据
 * @param generateTime
 * @param createTime
 * @param generateObjectCode
 * @param trajectoryList
 */
case class HiddenEvidence(var generateTime: String,
                          var createTime: String,
                          var generateObjectCode: String,
                          var trajectoryList: Array[HiddenTrajectory]
                         )

/**
 * 车辆行驶周期或者路线周期
 * @param times
 * @param vehicleNo
 * @param vehicleColor
 * @param driverStartTime
 * @param driverEndTime
 */
case class HiddenTrajectory(var times: String, var vehicleNo: String,var vehicleColor :String,var driverStartTime: String,var driverEndTime:String,code:String)

