package org.example.dao

/**
 * 风险规则报警配置
 * @param id
 * @param ruleId
 * @param warningTypeCode
 * @param warningFieldCode
 * @param expressionCode
 * @param fieldValue
 */
case class RiskRuleWarning(var id: Long,
                           var ruleId: Long,
                           var warningTypeCode: String,
                           var warningFieldCode: String,
                           var expressionCode: String,
                           var fieldValue: String
                          )
