package org.example.dao

import scala.beans.BeanProperty

/**
  *风险道路分布信息
  *
  * @param id 风险点id+路段id+统计日期
  * @param riskId 风险点id
  * @param roadId 路段id 暂定道路id+下划线+道路名拼接
  * @param roadName 路段名
  * @param adcode 城市编码
  * @param riskNumber 风险数
  * @param isHighRiskSection 是否高危路段
  * @param highRiskSection 高危路段集合
  * @param riskPointSet 风险点坐标集合
  * @param statisticalDay 统计日期 yyyy-MM-dd
  * @param updateTime 数据更新时间 yyyy-MM-dd HH:mm:ss
  */
case class RoadRiskDistributionIndex(@BeanProperty var id: String,
                                     @BeanProperty var riskId: String,
                                     @BeanProperty var roadId: String,
                                     @BeanProperty var roadName: String,
                                     @BeanProperty var adcode: String,
                                     @BeanProperty var riskNumber: Long,
                                     @BeanProperty var isHighRiskSection: String,
                                     @BeanProperty var highRiskSection: java.util.List[HighRiskSection],
                                     @BeanProperty var riskPointSet: java.util.List[RiskPoint],
                                     @BeanProperty var statisticalDay: String,
                                     @BeanProperty var updateTime: String
                                    )


/**
  * 高危路段集合
  *
  * @param highRiskRoadMileage 高发风险里程
  * @param direction 路段描述
  * @param highRiskRoad 高危路段
  */
case class HighRiskSection(
                            @BeanProperty var highRiskRoadMileage: Double,
                            @BeanProperty var direction: String,
                            @BeanProperty var highRiskRoad: java.util.List[HighRiskRoad]
                          )

/**
  *高危路段
  *
  * @param lon 高危路段的经度
  * @param lat 高危路段的纬度
  * @param address 高发风险中心点的位置
  */
case class HighRiskRoad(
                        @BeanProperty var lon: String,
                        @BeanProperty var lat: String,
                        @BeanProperty var address: String
                       )

/**
  *风险点坐标
  *
  * @param level 风险的等级
  * @param lon 风险的经度
  * @param lat 风险的纬度
  * @param address 风险发生位置
  */
case class RiskPoint(
                     @BeanProperty var level: String,
                     @BeanProperty var lon: String,
                     @BeanProperty var lat: String,
                     @BeanProperty var address: String
                    )