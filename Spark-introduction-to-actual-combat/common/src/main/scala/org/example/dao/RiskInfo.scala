package org.example.dao

/**
 * 风险点
 * @param id
 * @param riskName
 * @param riskTypeCode
 * @param isShortTimeHighFrequency
 * @param algorithmCycle
 */
case class RiskInfo(var id: Long,
                    var riskName: String,
                    var riskTypeCode: String,
                    var isShortTimeHighFrequency: Boolean,
                    var algorithmCycle: Option[Int],
                    //var vehicleTypes: Option[List[String]],
                    var useNatures: Option[List[String]],
                    var riskRules: Option[List[RiskRuleTrigger]]
                   )
