package org.example.dao

import scala.beans.BeanProperty

/**
 * 车辆实时风险指数
 * @param id
 * @param riskCode
 * @param vehicleNo
 * @param vehicleColor
 * @param useNature
 * @param driverName
 * @param driverCode
 * @param vehicleEnterprise
 * @param vehicleEnterpriseCode
 * @param riskDate
 * @param lastWarnAddress
 * @param carRiskIndex
 * @param risklevel
 * @param risklevelweight
 * @param updateTime
 */
case class RiskDangerIndex(@BeanProperty var id:String,
                           @BeanProperty var riskCode:String,
                           @BeanProperty var vehicleNo:String,
                           @BeanProperty var vehicleColor:String,
                           @BeanProperty var useNature:String,
                           @BeanProperty var driverName:String,
                           @BeanProperty var driverCode:String,
                           @BeanProperty var vehicleEnterprise:String,
                           @BeanProperty var vehicleEnterpriseCode:String,
                           @BeanProperty var riskDate:String,
                           @BeanProperty var lastWarnAddress:String,
                           @BeanProperty var carRiskIndex:Double,
                           @BeanProperty var risklevel:String,
                           @BeanProperty var risklevelweight:String,
                           @BeanProperty var updateTime:String
                          )
