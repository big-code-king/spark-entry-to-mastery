package org.example.utils

/**
  * 定位坐标系的转换类
  *
  *
  */
object TransformUtils {

  val x_PI = 3.14159265358979324 * 3000.0 / 180.0
  val PI = 3.14159265358979324
  //克拉索夫斯基椭球参数长半轴a
  val a = 6378245.0
  //克拉索夫斯基椭球参数第一偏心率平方
  val ee = 0.00669342162296594323

  /**
    * 火星坐标系 (GCJ-02) 与百度坐标系 (BD-09) 的转换 即谷歌、高德 转 百度
    *
    * @param lng 火星坐标系经度
    * @param lat 火星坐标系纬度
    * @return 百度坐标系经纬度
    */
  def gcj02tobd09(lng: Double, lat: Double): (Double, Double) = {
    val z: Double = Math.sqrt(lng * lng + lat * lat) + 0.00002 * Math.sin(lat * x_PI)
    val theta: Double = Math.atan2(lat, lng) + 0.000003 * Math.cos(lng * x_PI)
    val bd_lng: Double = z * Math.cos(theta) + 0.0065
    val bd_lat: Double = z * Math.sin(theta) + 0.006
    (bd_lng, bd_lat)
  }

  /**
    * GCJ02(火星坐标系) 转换为 WGS84(地球坐标系)
    *
    * @param lng 火星坐标系经度
    * @param lat 火星坐标系纬度
    * @return 地球坐标系经纬度
    */
  def gcj02towgs84(lng: Double, lat: Double): (Double, Double) = {
    var dlat: Double = transformlat(lng - 105.0, lat - 35.0)
    var dlng: Double = transformlng(lng - 105.0, lat - 35.0)
    val radlat: Double = lat / 180.0 * PI
    var magic: Double = Math.sin(radlat)
    magic = 1 - ee * magic * magic
    val sqrtmagic: Double = Math.sqrt(magic)
    dlat = (dlat * 180.0) / ((a * (1 - ee)) / (magic * sqrtmagic) * PI)
    dlng = (dlng * 180.0) / (a / sqrtmagic * Math.cos(radlat) * PI)
    val mglat: Double = lat + dlat
    val mglng: Double = lng + dlng
    (lng * 2 - mglng, lat * 2 - mglat)
  }

  /**
    * WGS84(地球坐标系)转GCj02(火星坐标系)
    *
    * @param lng 地球坐标系经度
    * @param lat 地球坐标系纬度
    * @return 火星坐标系经纬度
    */
  def wgs84togcj02(lng: Double, lat: Double): (Double, Double) = {
    var dlat: Double = transformlat(lng - 105.0, lat - 35.0)
    var dlng: Double = transformlng(lng - 105.0, lat - 35.0)
    val radlat: Double = lat / 180.0 * PI
    var magic: Double = Math.sin(radlat)
    magic = 1 - ee * magic * magic
    val sqrtmagic: Double = Math.sqrt(magic)
    dlat = (dlat * 180.0) / ((a * (1 - ee)) / (magic * sqrtmagic) * PI)
    dlng = (dlng * 180.0) / (a / sqrtmagic * Math.cos(radlat) * PI)
    val mglat: Double = lat + dlat
    val mglng: Double = lng + dlng

    (mglng, mglat)
  }


  /**
    * GCJ02(火星坐标系) 转换为 WGS84(地球坐标系) 纬度转换
    *
    * @param lng
    * @param lat
    * @return
    */
  def transformlat(lng: Double, lat: Double): Double = {
    var ret = -100.0 + 2.0 * lng + 3.0 * lat + 0.2 * lat * lat + 0.1 * lng * lat + 0.2 * Math.sqrt(Math.abs(lng))
    ret += (20.0 * Math.sin(6.0 * lng * PI) + 20.0 * Math.sin(2.0 * lng * PI)) * 2.0 / 3.0
    ret += (20.0 * Math.sin(lat * PI) + 40.0 * Math.sin(lat / 3.0 * PI)) * 2.0 / 3.0
    ret += (160.0 * Math.sin(lat / 12.0 * PI) + 320 * Math.sin(lat * PI / 30.0)) * 2.0 / 3.0
    ret
  }

  /**
    * GCJ02(火星坐标系) 转换为 WGS84(地球坐标系) 纬度转换
    *
    * @param lng
    * @param lat
    * @return
    */
  def transformlng(lng: Double, lat: Double): Double = {
    var ret = 300.0 + lng + 2.0 * lat + 0.1 * lng * lng + 0.1 * lng * lat + 0.1 * Math.sqrt(Math.abs(lng))
    ret += (20.0 * Math.sin(6.0 * lng * PI) + 20.0 * Math.sin(2.0 * lng * PI)) * 2.0 / 3.0
    ret += (20.0 * Math.sin(lng * PI) + 40.0 * Math.sin(lng / 3.0 * PI)) * 2.0 / 3.0
    ret += (150.0 * Math.sin(lng / 12.0 * PI) + 300.0 * Math.sin(lng / 30.0 * PI)) * 2.0 / 3.0
    ret
  }

  def main(args: Array[String]): Unit = {
    val lon_lat1: (Double, Double) = gcj02towgs84(106.265183,29.278646)
    println("lon_lat:" + lon_lat1._1 +","+lon_lat1._2)
  }
}
