package org.example.utils

import org.apache.commons.pool2.impl.GenericObjectPoolConfig
import org.example.constant.ApolloConst
import redis.clients.jedis.{Jedis, JedisPool, JedisPoolConfig}

/**
 * redis工具类
 */
object RedisUtils {

  private val host: String = ApolloConst.redisHost
  //private val host: String = "10.22.17.18"
  private val posrt: Int = 6379
  private val passWord: String = ApolloConst.redisPassword
  //private val passWord: String = "z123456"
  private val timeOut: Int = 10000


  /**
   *
   * 描述：初始化jedis连接池配置信息
   *
   * @return jedis连接池配置
   */
  private def initJedisPoolConfig(): GenericObjectPoolConfig = {

    val jedisPoolConfig = new JedisPoolConfig()

    // 最大空闲数
    jedisPoolConfig.setMaxIdle(300)
    // 连接池的最大数据库连接数
    jedisPoolConfig.setMaxTotal(2000)
    // 最大建立连接等待时间
    jedisPoolConfig.setMaxWaitMillis(1000)
    // 逐出连接的最小空闲时间 默认1800000毫秒(30分钟)
    jedisPoolConfig.setMinEvictableIdleTimeMillis(300000)
    // 每次逐出检查时 逐出的最大数目 如果为负数就是 : 1/abs(n), 默认3
    jedisPoolConfig.setNumTestsPerEvictionRun(1024)
    // 逐出扫描的时间间隔(毫秒) 如果为负数,则不运行逐出线程, 默认-1
    jedisPoolConfig.setTimeBetweenEvictionRunsMillis(30000)
    // 是否在从池中取出连接前进行检验,如果检验失败,则从池中去除连接并尝试取出另一个
    jedisPoolConfig.setTestOnBorrow(true)
    // 在空闲时检查有效性, 默认false
    jedisPoolConfig.setTestWhileIdle(true)
    jedisPoolConfig
  }

  /**
   *
   * 描述：获取jedis连接池
   *
   * @return
   */
  def createJedisPool(): JedisPool = {
    //  连接池配置 IP 端口号 超时时间 密码
    val pool: JedisPool = new JedisPool(initJedisPoolConfig(), host, posrt, timeOut, passWord)
    pool
  }

  /**
   *
   * 描述：获取jedis客户端
   *
   * @return
   */
  def getJedis(jedisPool: JedisPool): Jedis = {
    val jedis: Jedis = jedisPool.getResource
    jedis
  }


  def main(args: Array[String]): Unit = {

  }


}