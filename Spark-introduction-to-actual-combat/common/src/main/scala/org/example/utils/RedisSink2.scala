package org.example.utils

import org.example.common.Pattern
import org.example.constant.ApolloConst
import redis.clients.jedis.{Jedis, JedisPool, JedisPoolConfig}

class RedisSink2(makeJedisPool : () => JedisPool) extends Pattern with Serializable {

  lazy val pool: JedisPool = makeJedisPool()

  def usingRedis[A](execute:Jedis=>A):A = using(pool.getResource)(execute)
}

object RedisSink2 {

  def apply(): RedisSink = {
    val createJedisPoolFunc = () => {
      val config = new JedisPoolConfig
      val pool = new JedisPool(config,ApolloConst.redis2Host,ApolloConst.redis2Port,1000*2,ApolloConst.redis2Password)
      val hook = new Thread{
        override def run(): Unit = {pool.destroy()}
      }
      sys.addShutdownHook(hook)
      pool
    }
    new RedisSink(createJedisPoolFunc)
  }

  def main(args: Array[String]): Unit = {
    val sink = RedisSink()
    sink.usingRedis{x=>
      x.select(11)
      println(x.hget("airport","22|aa"))
    }
  }
}
