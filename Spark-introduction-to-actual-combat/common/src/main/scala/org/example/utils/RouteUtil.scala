package org.example.utils

/**
 * 路线工具类
 */
object RouteUtil {
  /**
   * 计算当前点位在路线点位中的类型，0未知点，1起始点，2途经点，3终点
   * @param routePointArr
   * @param lon
   * @param lat
   * @return
   */
  def getLocationType(routePointArr: Array[String], lon: String, lat: String) = {
    println("route"+routePointArr.mkString(";"))
    var driverGPS="-1,-1"
    var driverType="0"
    routePointArr.foreach(route=>{
      val routePointArr = route.split(";")
      if (routePointArr.length>0){
        for ( i <-  0 until routePointArr.length ){
          val routeLon = routePointArr(i).split(",")(0)
          val routeLat = routePointArr(i).split(",")(1)
          //计算两点之间的距离（米）
          val distance = Tools.getDistance(routeLon.toDouble, routeLat.toDouble, lon.toDouble, lat.toDouble)
          println("distance"+distance)
          if (distance<=1000.0){
            driverGPS=routeLon+","+routeLat
            if (i==0){
              driverType="1"
            }else if (i==routePointArr.length-1){
              driverType="3"
            }else{
              driverType="2"
            }
          }
        }
      }

    })



    (driverGPS,driverType)
  }

  /**
   * 计算当前点位在路线点位中的类型，0未知点，1起始点，2途经点，3终点
   * @param routePointArr
   * @param addrName
   * @return
   */
  def getAddrNameType(routePointArr: Array[String],addrName:String) = {
    var driverName="-1,-1"
    var driverType="0"
    routePointArr.foreach(route=>{
      val routePointArr = route.split(";")
      if (routePointArr.length>0){
        for ( i <-  0 until routePointArr.length ){
          val routeAddrName=routePointArr(i)
          if (IKUtil.ikMatch(addrName,routeAddrName)){
            driverName=routeAddrName
            if (i==0){
              driverType="1"
            }else if (i==routePointArr.length-1){
              driverType="3"
            }else{
              driverType="2"
            }
          }
        }
      }

    })



    (driverName,driverType)
  }

  /**
   * 计算包车牌当前点位在路线点位中的类型，0未知点，1起始点，2途经点，3终点
   * @param addrName
   * @param start_point_name
   * @param alignments
   * @param aim_name
   * @return
   */
  def getCharterLocationType(addrName:String,start_point_name:String,alignments:String,aim_name:String) = {
    println("addrName:"+addrName)
    var driverName="未知点"
    var driverType="0"

    if (IKUtil.ikMatch(addrName,aim_name)){
      driverName=aim_name
      driverType="3"
    }else {
      val alignmentsArr = alignments.split("[^\\u4e00-\\u9fa5a-zA-Z0-9（）]")
      alignmentsArr.foreach(s=>{
        if (IKUtil.ikMatch(addrName,s)){
          driverName=s
          driverType="2"
        }
      })
      if (driverType=="0"){
        if (IKUtil.ikMatch(addrName,start_point_name)){
          driverName=start_point_name
          driverType="1"
        }
      }
    }



    (driverName,driverType)
  }
}
