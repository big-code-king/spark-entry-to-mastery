package org.example.common

import scala.util.control.Exception.ignoring

/**
 * 规则接口类
 */
trait Pattern {

  type closeable = {def close(): Unit}

  def using[R <: closeable, T](resoucre: R)(execute: R => T): T = {
    try {
      execute(resoucre)
    } finally {
      ignoring(classOf[Throwable]) apply resoucre.close()
    }
  }
}
