package org.example.logic

import org.example.utils.SearchInfo
import org.apache.commons.lang3.StringUtils
import org.joda.time.format.DateTimeFormat
import org.joda.time.{DateTime, Seconds}
import redis.clients.jedis.Jedis

/**
  * 统计企业，车辆驾驶时长--从conmmonUtils移动到此
  *
  */
object EnterpriseDrivingProcess {

  /**
    * 统计企业，车辆驾驶时长（天）
    *
    * @param primaryKey
    * @param dateTime
    * @param jedis
    * @param speed
    */
  def enterpriseDrivingTimes(primaryKey: String, dateTime: String, speed: String, vehicleEnterpriseCode: String, jedis: Jedis): Unit = {
    val format = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")
    val expireTime = SearchInfo.getSecondsNextEarlyMorning().toInt

    //企业
    val key = "driverTime"
    val id = primaryKey
    //车辆所属公司编码
    val data = jedis.hget(key, primaryKey)
    val data1: String = jedis.hget("speedZero1", id)
    var data2 = ""

    try{
      data2 = jedis.hget("enterpriseTimes", vehicleEnterpriseCode)
    }catch {
      case _ =>
        null
    }
    val dateTime_n = DateTime.parse(dateTime, format)
    //车辆运行时长（车在计算周期内就累加）
    if (data != null) {
      val arr: Array[String] = data.split("\\|")
      val d_value: Int = arr(0).toInt
      val original_time: String = arr(1)
      val dateTime_o = DateTime.parse(original_time, format)
      val seconds = Seconds.secondsBetween(dateTime_o, dateTime_n).getSeconds
      var intervalSecond = seconds
      var oldTime = dateTime
      if (seconds < 0) {
        intervalSecond = 0
        oldTime = original_time
      }
      //断电不超过20分钟,或者速度为0不超过20分钟，则累计时间
      if (seconds > 20 * 60) {
        val cumulative_time = d_value
        val value = cumulative_time + "|" + oldTime
        jedis.hset(key, id, value)
        jedis.expire(key, expireTime)
      } else if (speed.toDouble > 0) {
        //断电不超过20分钟
        //时间累计
        val cumulative_time = intervalSecond + d_value
        val value = cumulative_time + "|" + oldTime
        jedis.hset(key, id, value)
        var mum = 0
        if (data1 != null) {
          if (speed.toDouble <= 20) {
            val zeroTimes: Array[String] = data1.split("\\|")
            val str = jedis.hget("allDay5Times", id)
            jedis.expire("allDay5Times", expireTime)
            if (str != null) {
              val str1: Array[String] = str.split(",")
              if (str1.size < 5) {
                jedis.hset("allDay5Times", id, str1.mkString(",") + "," + zeroTimes(1))
                jedis.expire("allDay5Times", expireTime)
              } else {
                val endseat = str1.drop(1)
                jedis.hset("allDay5Times", id, endseat.mkString(",") + "," + zeroTimes(1))
                jedis.expire("allDay5Times", expireTime)
              }
            } else {
              jedis.hset("allDay5Times", id, zeroTimes(1))
              jedis.expire("allDay5Times", expireTime)
            }
            mum = zeroTimes(2).toInt
            jedis.hset("speedZero1", id, zeroTimes(0) + "|" + zeroTimes(1) + "|" + (mum + 1))
            jedis.expire("speedZero1", expireTime)
          } else {
            //map重新置为空
            jedis.hset("speedZero1", id, 0 + "|" + oldTime + "|" + 0)
            jedis.expire("speedZero1", expireTime)
          }
        }
      }
      else if (data1 != null) {
        val arr1: Array[String] = data1.split("\\|")
        val speed_value: Int = arr1(0).toInt
        val speed_time: String = arr1(1)
        val num: Int = arr1(2).toInt
        val dateTime_speed = DateTime.parse(speed_time, format)
        val speedSeconds = Seconds.secondsBetween(dateTime_speed, dateTime_n).getSeconds
        var allTime = speed_value
        var curSpeedTime = speed_time
        if (speedSeconds > 0) {
          allTime = speed_value + speedSeconds
          curSpeedTime = dateTime
        }
        val timemap: String = jedis.hget("allDay5Times", id)
        if (timemap != null) {
          val timesarray = timemap.split(",")
          val datatime = DateTime.parse(timesarray(0), format)
          val seconds1: Int = Seconds.secondsBetween(datatime, dateTime_n).getSeconds
          if (seconds1 >= 20 * 60) {
            val value = d_value + "|" + oldTime
            jedis.hset(key, id, value)
            jedis.expire(key, expireTime)
          } else {
            //继续累计时长
            jedis.hset("speedZero1", id, allTime.toString + "|" + curSpeedTime + "|" + num)
            jedis.expire("speedZero1", expireTime)
          }
        } else {
          if (allTime >= 20 * 60 && num <= 5) {
            val value = d_value + "|" + oldTime
            jedis.hset(key, id, value)
            //周期重新累计时，删除挪车点
          } else {
            //新增速度为0判断
            jedis.hset("speedZero1", id, allTime.toString + "|" + curSpeedTime + "|" + num)
            jedis.expire("speedZero1", expireTime)
          }
        }

      } else {
        jedis.hset("speedZero1", id, "0|" + oldTime + "|0")
        jedis.expire("speedZero1", expireTime)
      }
    } else {
      jedis.hset(key, id, 0 + "|" + dateTime)
      jedis.expire(key, expireTime)
    }


    //企业运行时长（只有有车在计算周期内就累加）
    if (StringUtils.isNotEmpty(data2)) {
      val arr: Array[String] = data2.split("\\|")
      //println("vehicleEnterprise----------------" + vehicleEnterpriseCode)
      val en_value: Int = arr(0).toInt
      val enterprise_time: String = arr(1)
      val dateTime_e = DateTime.parse(enterprise_time, format)
      val seconds = Seconds.secondsBetween(dateTime_e, dateTime_n).getSeconds
      var enIntervalSecond = seconds
      var oldTime = dateTime
      if (seconds < 0) {
        enIntervalSecond = 0
        oldTime = enterprise_time
      }
      var cumulative_time = enIntervalSecond + en_value
      var value = cumulative_time + "|" + oldTime
      //断电超过20分钟,时间不累计
      if (seconds > 20 * 60) {
        cumulative_time = en_value
        value = cumulative_time + "|" + oldTime
        jedis.hset("enterpriseTimes", vehicleEnterpriseCode, value)
        jedis.expire("enterpriseTimes", expireTime)
      } else if (speed.toDouble > 0) {
        jedis.hset("enterpriseTimes", vehicleEnterpriseCode, value)
        jedis.expire("enterpriseTimes", expireTime)
      }
      else if (data1 != null) {
        val arr1: Array[String] = data1.split("\\|")
        val speed_value: Int = arr1(0).toInt
        val speed_time: String = arr1(1)
        val num: Int = arr1(2).toInt
        val dateTime_speed = DateTime.parse(speed_time, format)
        val speedSeconds = Seconds.secondsBetween(dateTime_speed, dateTime_n).getSeconds
        var speed_newTime = speed_value
        var curSpeedTime = speed_time
        if (speedSeconds < 0) {
          speed_newTime = speed_value + speedSeconds
          curSpeedTime = dateTime
        }
        val timemap: String = jedis.hget("allDay5Times", id)
        if (timemap != null) {
          val timesarray = timemap.split(",")
          val datatime = DateTime.parse(timesarray(0), format)
          val seconds1: Int = Seconds.secondsBetween(datatime, dateTime_n).getSeconds
          if (seconds1 >= 20 * 60) {
            value = en_value +"|" + oldTime
            jedis.hset("enterpriseTimes", vehicleEnterpriseCode, value)
            jedis.expire("enterpriseTimes", expireTime)
            jedis.hdel("allDay5Times", id)
          }
        }else {
          if(speed_newTime >= 20 * 60 && num <= 5){
            value = en_value +"|" + oldTime
            jedis.hset("enterpriseTimes", vehicleEnterpriseCode, value)
            jedis.expire("enterpriseTimes", expireTime)
            jedis.hdel("allDay5Times", id)
          }
        }
      }
    } else {
      if (null != vehicleEnterpriseCode) {
        jedis.hset("enterpriseTimes", vehicleEnterpriseCode, 0 + "|" + dateTime)
        jedis.expire("enterpriseTimes", expireTime)
      }
    }
  }

}
