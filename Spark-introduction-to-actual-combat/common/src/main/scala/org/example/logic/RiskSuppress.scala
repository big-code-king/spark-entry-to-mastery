package org.example.logic

import com.alibaba.fastjson.JSONObject
import org.example.utils.CommonUtils
import org.apache.commons.lang3.StringUtils
import redis.clients.jedis.Jedis
import org.example.cache.BasicInformationCache.{error, warn}

object RiskSuppress {

  /**
    * 更新车辆风险抑制信息
    *
    * @param messageInfo
    * @param jedis
    */
  def updateRiskSuppressTime(messageInfo: JSONObject, jedis: Jedis): Unit = {
    //车牌号
    val vehicleNo: String = messageInfo.getString("vehicleNo")
    //车牌颜色
    val vehicleColor: String = messageInfo.getString("vehicleColor")
    //风险点id
    val riskId: String = messageInfo.getString("riskId")
    //风险等级 1较小风险,2一般风险,3较大风险,4重大风险
    val riskGrade: String = messageInfo.getString("riskGrade")
    //抑制结束时间 格式yyyy-MM-dd HH:mm:ss
    val suppressEndtime: String = messageInfo.getString("suppressEndtime")

    //存抑制结束时间
    if (StringUtils.isNotEmpty(vehicleNo)
      && StringUtils.isNotEmpty(vehicleColor)
      && StringUtils.isNotEmpty(riskId)
      && StringUtils.isNotEmpty(riskGrade)
      && StringUtils.isNotEmpty(suppressEndtime)) {
      val key = "riskSuppress:" + vehicleNo + "_" + vehicleColor + "_" + riskId + "_" + riskGrade
      try {
        //获取过期时间
        val expireSeconds = CommonUtils.getTimeDifference(suppressEndtime)
        if(expireSeconds > 0){
          jedis.set(key, suppressEndtime)
          jedis.expire(key, expireSeconds)
        }else{
          warn(s"车辆${vehicleNo}抑制已经结束,无需保存,抑制结束时间：${suppressEndtime}")
        }
      }catch {
        case e: Exception => {
          error(s"车辆${vehicleNo}抑制结束时间格式不对：${suppressEndtime}")
        }
      }
    }
  }

  /**
    * 判断当前风险点是否处于抑制状态
    * @param key
    * @param redis
    * @return
    */
  def isRiskSuppress(key: String, redis: Jedis): Boolean ={
    var isSuppress = false
    //抑制结束时间 格式yyyy-MM-dd HH:mm:ss
    val suppressEndtime = redis.get(key)
    if(StringUtils.isNotEmpty(suppressEndtime)){
      val seconds = CommonUtils.getTimeDifference(suppressEndtime)
      if(seconds > 0){
        isSuppress = true
      }
    }
    isSuppress
  }


}
