package org.example.originUtils

import scala.collection.mutable

/**
  * 设备类的报警信息
  */
object BaseWarn {
  val warnMap = new mutable.HashMap[Tuple2[String, String], Tuple2[String, String]]()
  warnMap.put(("0x0064", "0x01"), ("前向碰撞报警", "forwardCollision"))
  warnMap.put(("0x0064", "0x02"), ("车道偏离报警", "laneDeparture"))
  warnMap.put(("0x0064", "0x03"), ("车距过近报警", "tooClose"))
  warnMap.put(("0x0064", "0x04"), ("行人碰撞报警", "pedestrianCollision"))
  warnMap.put(("0x0064", "0x05"), ("频繁变道报警", "frequentLaneChange"))
  warnMap.put(("0x0064", "0x06"), ("道路标识超限报警", "roadDignsExceedTheLimit"))
  warnMap.put(("0x0064", "0x07"), ("路口快速通过报警", "quickPassageThroughTheIntersection"))
  warnMap.put(("0x0064", "0x10"), ("道路标志识别事件", "roadSignRecognitionEvent"))
  warnMap.put(("0x0064", "0x11"), ("主动抓拍事件", "activeCaptureOfEvents"))
  warnMap.put(("0x0065", "0x01"), ("疲劳驾驶报警", "fatigueDriving"))
  warnMap.put(("0x0065", "0x02"), ("接打手持电话报警", "answerHandheldPhoneCalls"))
  warnMap.put(("0x0065", "0x03"), ("抽烟报警", "smoke"))
  warnMap.put(("0x0065", "0x04"), ("分神驾驶报警", "distractedDriving"))
  warnMap.put(("0x0065", "0x05"), ("驾驶员异常报警", "driverAbnormality"))
  warnMap.put(("0x0065", "0x06"), ("探头遮挡报警", "probeOcclusion"))
  warnMap.put(("0x0065", "0x07"), ("换人驾驶报警", "substitutionDriving"))
  warnMap.put(("0x0065", "0x08"), ("超时驾驶报警", "overtimeDriving"))
  warnMap.put(("0x0065", "0x09"), ("驾驶员人脸身份识别事件", "driverFaceIdentityRecognitionEvent"))
  warnMap.put(("0x0065", "0x10"), ("自动抓拍事件", "autoCaptureEvents"))
  warnMap.put(("0x0066", "0x01"), ("后方接近报警", "rearApproach"))
  warnMap.put(("0x0066", "0x02"), ("左侧后方接近报警", "leftRearApproach"))
  warnMap.put(("0x0066", "0x03"), ("右侧后方接近报警", "rightRearApproach"))

  //贵阳adas和dsm报警数据类型--区别于杭州--存于db_tbd_base1.warning_type表中
  val warnTypeMap = new mutable.HashMap[String, Tuple2[String,String]]()
  // TODO: v1.1版本所上管控的报警类型
  warnTypeMap.put("0x00103", ("车道偏离报警", "laneDeparture"))
  warnTypeMap.put("0x00104", ("车距过近报警", "tooClose"))
  warnTypeMap.put("0x00102", ("前向碰撞报警", "forwardCollision"))
  warnTypeMap.put("0x00107", ("分神报警", "distractedDriving"))
  warnTypeMap.put("0x00f", ("抽烟报警", "smoke"))
  warnTypeMap.put("0x00e", ("使用手机报警", "answerHandheldPhoneCalls"))
  warnTypeMap.put("0x002", ("疲劳驾驶报警", "fatigueDriving"))
  warnTypeMap.put("0x0011", ("驾驶人异常报警", "driverAbnormality"))
  //

  /**
    * 获取报警字典
    *
    * @param bigType   kafka消息中的dataType
    * @param smallType kafka消息中的eventType
    * @return
    */
  def getBaseWarnByType(bigType: String, smallType: String): (String, String) = {
    warnMap.getOrElse((bigType,smallType),(null,null))
  }

  /**
   * @Description
   * @Date  2021/7/22
   * @Param [warnType:kafka消息中的warnType]
   * @return scala.Tuple2<java.lang.String,java.lang.String>
   **/
  def getWarnTypeByCode(warnType: String): (String, String) = {
    warnTypeMap.getOrElse(warnType,(null,null))
  }
}
