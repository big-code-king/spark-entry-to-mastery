package org.example.constant

/**
 * @Description 风险常量
 **/
object RiskConst {
  // 报警级别
  val WARNING_LEVEL_FIRST = "1"
  // 报警信息来源-其他
  val WARNING_SOURCE = "9"
  //风险处理状态-未处理
  val PROCESS_STATUS_UNTREATED = "0"
  // 风险处理状态-抑制
  val PROCESS_STATUS_SUPPRESS = "11"
  // 风险有效性 1-有效 0-无效
  val RISK_STATUS_VALID = "1"
  // 风险抑制状态 1-抑制 0-不抑制
  val RISK_STATUS_HOLD = "1"
  // 六十码阀值
  val SIXTY_THRESHOLD = 60.0D
  // 八十码阀值
  val EIGHTY_THRESHOLD = 80.0D
  // 百码阀值
  val ONE_HUNDRED_THRESHOLD = 100.0D
  // 一百二十码阀值
  val ONE_HUNDRED_AND_TWENTY_THRESHOLD = 120.0D
  //固定报警间隔周期 30s
  val INTERVAL_PERIOD_FORTY_SECOND = 30
}
