package org.example.constant

import com.ctrip.framework.apollo.{Config, ConfigService}

/**
 * 阿波罗配置常量
 */
object ApolloConst {
  System.setProperty("app.id", "yychdsjzx2022")
  System.setProperty("apollo.meta", "http://10.197.236.232:7070")
  val config: Config = ConfigService.getConfig("application")
  val hiveMetastore: String = config.getProperty("hive.metastore.uris", null)

  //hdfs连接信息
  var hdfsUrl: String = config.getProperty("hdfs.url", null)
  var FSName: String = config.getProperty("fs.defaultFS", null)
  var hdfsName: String = config.getProperty("dfs.nameservices", null)
  var hdfsNodes: String = config.getProperty("dfs.ha.namenodes.nameservices", null)
  var hdfsNode1: String = config.getProperty("dfs.namenode.rpc-address.nameservices.nn1", null)
  var hdfsNode2: String = config.getProperty("dfs.namenode.rpc-address.nameservices.nn2", null)
  var nn1Address: String = config.getProperty("nn1.address", null)
  var nn2Address: String = config.getProperty("nn2.address", null)
  val queueSize: Int = config.getIntProperty("queue.size", 100)
  //kafka基本信息
  val zkKafka: String = config.getProperty("kafka.zookerper.servers", null)
  val bootstrap: String = config.getProperty("kafka.bootstrap.servers", null)
  val groupId: String = config.getProperty("kafka.groupId.servers", null)
  //es连接信息
  val esNodes:String = config.getProperty("es.nodes",null)
  val esPort:String = config.getProperty("es.port",null)

  //hive数仓jdbc连接信息
  val hiveURL: String = config.getProperty("hive.url", null)
  val hiveDriver: String = config.getProperty("hive.driver", "org.apache.hive.jdbc.HiveDriver")
  val hiveUserName: String = config.getProperty("hive.uesrname", "")
  val hivePassWord: String = config.getProperty("hive.password", "")

  //redis
  val redisHost: String = config.getProperty("redis.host", null)
  val redisPort: Int = config.getIntProperty("redis.port", null)
  val redisPassword: String = config.getProperty("redis.password", null)

  val redis2Host: String = config.getProperty("redis2.host", null)
  val redis2Port: Int = config.getIntProperty("redis2.port", null)
  val redis2Password: String = config.getProperty("redis2.password", null)

  //数据接入hdfs的地址
  val onlineCarOrderHdfs: String = config.getProperty("onlinecar.order.hdfs", null)
  // "/data/online_car_order/"
  val onlineCarInfoHdfs: String = config.getProperty("onlinecar.info.hdfs", null)

  //逆地理接口
  val reverseGeo:String=config.getProperty("reverse.geocoding",null)

  //保存单车最后一条gps GroupId
  val LastGps_Group = config.getProperty("lastGps.group", null)

  //企业数据录入信息表 GroupId
  val enterpriseDataEnter_Group = config.getProperty("enterpriseDataEnter.group", null)

  // 企业gps数据
  val ALLGPS_GROUP: String = config.getProperty("allGps.group",null)
  val GPS_TOPICS:Seq[String] = config.getProperty("gps.topics",null).split(",")
  val RISK_GROUP = config.getProperty("risk.group",null)

  // 监管端数据库配置信息
  //mysql
  val jgdMysqlURL: String = config.getProperty("jgd.mysql.url", null)
  val jgdMysqlUserName: String = config.getProperty("jgd.mysql.username", null)
  val jgdMysqlPassWord: String = config.getProperty("jgd.mysql.password", null)
  val jgdMysqlDriver: String = config.getProperty("jgd.mysql.driver", null)
  //AmapKey地图定位key
  val amapKey = config.getProperty("AmapKey",null)  //ec876713e6731f8e1a9b2fa4af1c3df3
  val amapByAddressKey = config.getProperty("AmapByAddressKey",null)  //ec876713e6731f8e1a9b2fa4af1c3df3

  //产生隐患
  val hiddenPerilModelGroup = config.getProperty("hiddenPerilModel.group",null) //产生隐患的kafka消费者组
  val hiddenPerilModelTopic = config.getProperty("hiddenPerilModel.topic",null) //产生隐患的kafka主题

  //报警视频
  val warnattach_group = config.getProperty("warnattach.group",null)
  val warnattach_topic = config.getProperty("warnattach.topic",null)
  //报警详情
  val alarmdetails_group = config.getProperty("alarmdetails.group",null)
  val alarmdetails_topic = config.getProperty("alarmdetails.topic",null)


  //行驶里程group
  val trafficDayGroup = config.getProperty("traffic.group",null)
  // gps数据
  val LAST_GPS_TOPICS: Seq[String] = config.getProperty("last.gps.topics", null).split(",")

  val noRiskLevelWeight = config.getProperty("noRiskLevelWeight",null)
  val lowRiskLevelWeight = config.getProperty("lowRiskLevelWeight",null)
  val midRiskLevelWeight = config.getProperty("midRiskLevelWeight",null)
  val highRiskLevelWeight = config.getProperty("highRiskLevelWeight",null)
  val noRiskThreshold = config.getProperty("noRiskThreshold",null)
  val lowRiskThreshold = config.getProperty("lowRiskThreshold",null)
  val midRiskThreshold = config.getProperty("midRiskThreshold",null)
  val highRiskThreshold = config.getProperty("highRiskThreshold",null)

  //高危路段参数值
  val unitRadius: Double=config.getProperty("unitRadius",null).toDouble
  val densityThreshold: Int=config.getProperty("densityThreshold",null).toInt

  //风险路段参数值
  val riskUnitRadius: Double=config.getProperty("risk.unit.radius", "1.5").toDouble
  val riskDensityThreshold: Int=config.getProperty("risk.density.threshold", "5").toInt
  val riskDrivingFactor: Double=config.getProperty("risk.driving.factor", "1").toDouble
  val highDrivingLoadFactor: Double=config.getProperty("high.driving.load.factor", "0.4").toDouble
  val riskDrivingLoadFactor: Double=config.getProperty("risk.driving.load.factor", "0.75").toDouble

  val areaCodeAndAddressUrl:String=config.getProperty("areaCodeAndAddress.Url",null)
}
