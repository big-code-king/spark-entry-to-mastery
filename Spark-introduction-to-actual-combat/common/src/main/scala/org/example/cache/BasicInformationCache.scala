package org.example.cache

import com.alibaba.fastjson.JSONObject
import org.apache.commons.lang3.StringUtils
import org.example.common.Logging
import redis.clients.jedis.Jedis

object BasicInformationCache extends Logging{
  /**
    * 更新车辆基本信息
    *
    * @param messageInfo
    * @param jedis
    */
  def updateVehicleInfo(messageInfo: JSONObject, jedis: Jedis): Unit = {
    if (messageInfo.containsKey("plateNum") && messageInfo.containsKey("plateColor")) {
      val vehicleNo: String = messageInfo.getString("plateNum")
      val vehicleColor: String = messageInfo.getString("plateColor")
      if (StringUtils.isNotEmpty(vehicleNo) && StringUtils.isNotEmpty(vehicleColor)) {
        val primaryKey = vehicleNo + "#" + vehicleColor
        val operationType = messageInfo.getString("operationType")

        //增加一条车辆信息时
        if ("add".equals(operationType)) {
          var useNature: String = messageInfo.getString("useNature")
          val enterpriseCode: String = messageInfo.getString("enterpriseCode")
          //车辆管控类型
          val controlType = messageInfo.getString("controlType")
          if (StringUtils.isEmpty(useNature)) {
            useNature = "-"
          }
          jedis.hset("driverInfo:" + primaryKey, "useNature", useNature)
          jedis.hset("driverInfo:" + primaryKey, "vehicleEnterprise", enterpriseCode)
          jedis.hset("driverInfo:" + primaryKey, "controlType", controlType)
          jedis.hset("monitorVehicle", primaryKey, "1")
        } else if ("update".equals(operationType)) {
          val status = messageInfo.getString("status")
          if ("NORMAL".equals(status)) {
            var useNature: String = messageInfo.getString("useNature")
            val enterpriseCode: String = messageInfo.getString("enterpriseCode")
            //车辆管控类型
            val controlType = messageInfo.getString("controlType")
            if (StringUtils.isEmpty(useNature)) {
              useNature = "-"
            }
            jedis.hset("driverInfo:" + primaryKey, "useNature", useNature)
            jedis.hset("driverInfo:" + primaryKey, "vehicleEnterprise", enterpriseCode)
            jedis.hset("driverInfo:" + primaryKey, "controlType", controlType)
            jedis.hset("monitorVehicle", primaryKey, "1")
          } else if ("INITIAL".equals(status)) {
            jedis.del("driverInfo:" + primaryKey)
            jedis.hdel("lastOnlineSmartDay", primaryKey)
            jedis.hdel("lastOnlineGpsDay", primaryKey)
            jedis.hdel("monitorVehicle", primaryKey)
          }
        } else if ("delete".equals(operationType)) {
          jedis.del("driverInfo:" + primaryKey)
          jedis.hdel("lastOnlineSmartDay", primaryKey)
          jedis.hdel("lastOnlineGpsDay", primaryKey)
          jedis.hdel("monitorVehicle", primaryKey)
        }

        //删除一辆车时
        val isDeleted = messageInfo.getString("isDeleted")
        if ("true".equals(isDeleted)) {
          jedis.del("driverInfo:" + primaryKey)
          jedis.hdel("lastOnlineSmartDay", primaryKey)
          jedis.hdel("lastOnlineGpsDay", primaryKey)
          jedis.hdel("monitorVehicle", primaryKey)
        }
      }
    }

  }

  /**
    * 更新驾驶员基本信息
    *
    * @param messageInfo
    * @param jedis
    */
  def updateDriverInfo(messageInfo: JSONObject, jedis: Jedis): Unit = {
    val licenseNumber: String = messageInfo.getString("licenseNumber")
    if (StringUtils.isNotEmpty(licenseNumber)) {
      val operationType = messageInfo.getString("operationType")
      val driverEnterprise: String = messageInfo.getString("enterpriseCode")
      var telephone: String = messageInfo.getString("telephone")
      if (StringUtils.isEmpty(telephone)) {
        telephone = "-"
      }
      //增加一条车辆信息时
      if ("add".equals(operationType)) {
        if (StringUtils.isNotEmpty(driverEnterprise)) {
          jedis.hset("licenseNumber:" + licenseNumber, "driverEnterprise", driverEnterprise)
        }
        jedis.hset("licenseNumber:" + licenseNumber, "telephone", telephone)
      } else if ("update".equals(operationType)) {
        if (StringUtils.isNotEmpty(driverEnterprise)) {
          jedis.hset("licenseNumber:" + licenseNumber, "driverEnterprise", driverEnterprise)
        }
        jedis.hset("licenseNumber:" + licenseNumber, "telephone", telephone)
      } else if ("delete".equals(operationType)) {
        jedis.del("licenseNumber:" + licenseNumber)
      }

      //删除一个驾驶员时
      val isDeleted = messageInfo.getString("isDeleted")
      if ("true".equals(isDeleted)) {
        jedis.del("licenseNumber:" + licenseNumber)
      }
    }
  }

  /**
    * 更新平台基本信息
    *
    * @param messageInfo
    * @param jedis
    */
  def updatePlatformInfo(messageInfo: JSONObject, jedis: Jedis): Unit = {
    val platformCode: String = messageInfo.getString("platformCode")
    val platformName: String = messageInfo.getString("platformName")

    if (StringUtils.isNotEmpty(platformCode) && StringUtils.isNotEmpty(platformName)) {
      val operationType = messageInfo.getString("operationType")
      //增加一条平台信息时
      if ("add".equals(operationType)) {
        jedis.hset("platform", platformCode, platformName)
      } else if ("update".equals(operationType)) {
        val status = messageInfo.getString("status")
        if ("NORMAL".equals(status)) {
          jedis.hset("platform", platformCode, platformName)
        } else if ("INITIAL".equals(status)) {
          jedis.hdel("platform", platformCode)
        }
      } else if ("delete".equals(operationType)) {
        jedis.hdel("platform", platformCode)
      }
    }
  }

  /**
    * 更新企业基本信息
    *
    * @param messageInfo
    * @param jedis
    */
  def updateEnterpriseInfo(messageInfo: JSONObject, jedis: Jedis): Unit = {
    val enterpriseCode: String = messageInfo.getString("enterpriseCode")
    val enterpriseName: String = messageInfo.getString("enterpriseName")

    if (StringUtils.isNotEmpty(enterpriseCode) && StringUtils.isNotEmpty(enterpriseName)) {
      val operationType = messageInfo.getString("operationType")
      //增加一条车辆信息时
      if ("add".equals(operationType)) {
        jedis.hset("enterprise", enterpriseCode, enterpriseName)
      } else if ("update".equals(operationType)) {
        val status = messageInfo.getString("status")
        if ("NORMAL".equals(status)) {
          jedis.hset("enterprise", enterpriseCode, enterpriseName)
        } else if ("INITIAL".equals(status)) {
          jedis.hdel("enterprise", enterpriseCode)
        }
      } else if ("delete".equals(operationType)) {
        jedis.hdel("enterprise", enterpriseCode)
      }
    }
  }

}
