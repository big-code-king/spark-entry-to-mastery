package org.example.model;

/**
 * 位置信息实体类
 */
public class LocateInfo {
    /**
     * 经度
     */
    private double longitude;
    /**
     * 维度
     */
    private double Latitude;
    /**
     * 是否在中国
     */
    private boolean isChina;

    public LocateInfo() {
    }

    public LocateInfo(double longitude, double latitude, boolean isChina) {
        this.longitude = longitude;
        Latitude = latitude;
        this.isChina = isChina;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return Latitude;
    }

    public void setLatitude(double latitude) {
        Latitude = latitude;
    }

    public boolean isChina() {
        return isChina;
    }

    public void setChina(boolean china) {
        isChina = china;
    }
}
