package org.example.model;

import com.alibaba.fastjson.JSON;

import java.util.HashMap;
import java.util.List;

/**
 * 集合转json
 */
public class MapToJson {
    public static String listMap2Josn(List<HashMap<String,String>> ll) {
        String string = JSON.toJSONString(ll);
        return string;
    }


    public static String Map2Josn(HashMap<String,String> ll) {
        String string = JSON.toJSONString(ll);
        return string;
    }

    public static String map2Josn(HashMap<String,Object> ll) {
        String string = JSON.toJSONString(ll);
        return string;
    }
}
