package org.example.udf;

import org.apache.hadoop.hive.ql.exec.UDF;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * hive自定义函数 日期格式标准化  转为yyyy-MM-dd
 */
public class DateStandardization extends UDF {
    //isSupplement : 值为"1"时：补充不完整的月份和日期，填补的值为1
    public String evaluate(String str,String isSupplement ){
        String regex1 = "(\\d{4})(?:(\\d{2})(\\d{2})?)?";   //2020 202002 20200202
        String regex2 = "(\\d{4})[.-](\\d{1,2})(?:[.-](\\d{1,2}))?"; //2020-02-02 2020-2-2 2020-2 2020.2 2020.02.02 2020.2.2
        String regex3 = "(\\d{4})年(?:(\\d{1,2})月(?:(\\d{1,2})日)?)?";//2020年 2020年02月 2020年02月02日 2020年2月 2020年2月2日
        Pattern pattern;
        if(str.matches(regex1)){
            pattern = Pattern.compile(regex1);
        }else if(str.matches(regex2)){
            pattern = Pattern.compile(regex2);
        }else if(str.matches(regex3)){
            pattern = Pattern.compile(regex3);
        }else{
            return "";
        }

        Matcher matcher = pattern.matcher(str);
        matcher.find();
        String y=matcher.group(1);
        String m=matcher.group(2);
        String d=matcher.group(3);

        String mStr;
        String dStr;
        if(y==null || y.isEmpty()){return "";}
        if(m==null || m.isEmpty()) {
            if("1".equals(isSupplement)) {
                mStr = "-01";
            }else{
                mStr = "";
            }
        } else{
            mStr=String.format("-%02d",Integer.parseInt(m));
        }

        if(d==null || d.isEmpty()) {
            if("1".equals(isSupplement)) {
                dStr="-01";
            }else{
                dStr = "";
            }
        } else{
            dStr=String.format("-%02d",Integer.parseInt(d));
        }

        return y+mStr+dStr;
    }

    public static void main(String[] args) {
        System.out.println(new DateStandardization().evaluate("2020.2","1"));
        System.out.println(new DateStandardization().evaluate("2020年","1"));
    }
}
