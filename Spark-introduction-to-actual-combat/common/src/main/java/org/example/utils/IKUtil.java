package org.example.utils;

import org.wltea.analyzer.core.IKSegmenter;
import org.wltea.analyzer.core.Lexeme;

import java.io.IOException;
import java.io.StringReader;

public class IKUtil {
    public static boolean ikMatch(String matchString,String ikString)
    {
        System.out.println(matchString+"_"+ikString);
        StringReader sr=new StringReader(ikString);
        IKSegmenter ik=new IKSegmenter(sr, true);
        Lexeme lex=null;
        boolean flag =true;
        while(true){
            try {
                if (!((lex=ik.next())!=null)) break;
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            System.out.print(lex.getLexemeText()+"|");
            if (!matchString.contains(lex.getLexemeText())){
                flag=false;
            }
        }
        System.out.println("");
        return flag;
    }

    public static void main(String[] args) {
        System.out.println(ikMatch("123","123"));
    }
}
